# coding=utf-8

import graphviz as gvz
import networkx as nx

def safe_format( node_str: str ) -> str:
    return node_str.replace( ":", ";" )

def print_graph( graph, filename="graph", cleanup=True ) :
    # Only use the property of the graph, directly working on a networkx graph
    dot = gvz.Digraph(format='pdf')

    for node in graph.nodes :
        dot.node( safe_format(str( node )), safe_format(str( node )) )

    if graph.is_multigraph() :
        for s, t, _ in graph.edges :
            dot.edge( safe_format(str( s )), safe_format(str( t )) )
    else :
        for s, t in graph.edges :
            dot.edge( safe_format(str( s )), safe_format(str( t )) )

    dot.render( filename, cleanup=cleanup )

def print_wcet_path( wcet_path, icfg_graph, filename="graph", cleanup=True ):
    dot = gvz.Digraph(format='pdf')

    def id_to_fname( id: str ):
        if id in icfg_graph.nodes:
            return icfg_graph.nodes[ id ][ "cfg" ].name
        else:
            return id

    def transform_block_name( id : str ):
        s = id.split(":")
        s[0] = id_to_fname( s[0] )
        return ";".join( s )

    for node in wcet_path.nodes :
        dot.node( transform_block_name(str( node )), transform_block_name(str( node )) )

    for s, t in wcet_path.edges :
        dot.edge( transform_block_name( str( s ) ), transform_block_name( str( t ) ) )

    dot.render( filename, cleanup=cleanup )