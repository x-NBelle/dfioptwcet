# coding=utf-8
from enum import IntEnum
from pathlib import Path
from configurations import config


class Color( IntEnum ):
    BLACK = 30
    RED = 31
    GREEN = 32
    ORANGE = 33
    BLUE = 34
    MAGENTA = 35
    CYAN = 36
    LIGHT_GRAY = 37
    DEFAULT = 39

def print_color( s: str, color: Color, *args, **kwargs ):
    print( color_str(s, color), *args, **kwargs )

def color_str( s: str, color: Color ) -> str:
    return f"\e[1;{color}m{s}\e[0m"