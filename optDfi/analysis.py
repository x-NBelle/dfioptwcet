# coding=utf-8
from itertools import chain
from pathlib import Path
from typing import NamedTuple, List, Dict, Tuple

import optDfi.interface.objdump as obj
from optDfi.interface import a3, llvm
from optDfi.interface.a3.misc import ContextID, ContextInfo, ModuloInfo
from optDfi.interface.a3.a3 import InterfaceAiT
from optDfi.interface.a3.ICFG import ICFG


class ExtendedDFILoad( NamedTuple ) :
    bin_info: obj.DFILoad
    a3_fid: str
    a3_bbid_start: str
    a3_bbid_checks: List[ str ]
    a3_bbid_end: str


class ExtendedDFIStore( NamedTuple ) :
    bin_info: obj.DFIStore
    a3_fid: str
    a3_bbid: str


class LoadAllContextInfo( NamedTuple ) :
    load: ExtendedDFILoad
    context_info: Dict[ ContextID, ContextInfo ]
    fully_known: bool  # Do we have for each context only one value (but maybe different between the context)
    partially_known: bool  # Is there some context in which we know better than the load_set
    singleton_value: bool  # Is there only one tag possible for this load


def find_certain_add_read( interface: InterfaceAiT, address: int ) -> List[ str ] :
    value_insts = interface._get_value_instruction( address )
    return list( map( lambda r : r[ 0 ],
                      filter( lambda r : r[ 2 ] == "32",
                              map(
                                  lambda area : (area.attrib[ "min" ], area.attrib[ "max" ], area.attrib[ "mod" ]),
                                  chain( *(map(
                                      lambda inst : inst.findall( "./a3:value_step/a3:value_area", a3.NS ),
                                      value_insts )) ) ) ) ) )


def analyze_dfi_load_values( interface: InterfaceAiT, load: ExtendedDFILoad ) -> LoadAllContextInfo :
    fully_known = True
    partially_known = False
    singleton_value = True
    context_info: Dict[ ContextID, ContextInfo ] = { }

    load_tag_set = set( map( lambda c : c.tag, load.bin_info.check_data ) )

    if len( load_tag_set ) == 1 :
        return LoadAllContextInfo( load=load, context_info={ }, fully_known=True, partially_known=True,
                                   singleton_value=True )

    value_blocks = interface._findall_xpath(
        [ a3.misc.XPATH_VALUE_INSTRUCTION + "[@address='{add}']".format( add=hex( load.bin_info.start_add ) ),
          ".." ] )

    singleton_value_data = None
    for context_block in value_blocks :
        context = context_block.attrib[ "context_id" ]
        value_steps = context_block.findall(
            "/a3:".join( [ ".", "value_instruction[@address='{add}']".format( add=hex( load.bin_info.start_add ) ),
                           "value_step" ] ), a3.misc.NS )

        values = set()
        area_set = set()
        for step in value_steps :
            intervals = step.findall( "/a3:".join( [ ".", "value_step_content", "value_interval" ] ), a3.misc.NS )
            area = step.findall( "/a3:".join( [ ".", "value_area" ] ), a3.misc.NS )

            for area_data in area :
                minimum = int( area_data.attrib[ "min" ], base=16 )
                maximum = int( area_data.attrib[ "max" ], base=16 )
                mod = int( area_data.attrib.get( "mod", 0 ) )
                rem = int( area_data.attrib.get( "rem", hex( minimum ) ), base=16 )
                area_set.add( ModuloInfo( min=minimum, max=maximum, mod=mod, rem=rem ) )

            # The case where we have no information at all
            # This can be because we do not have a sharp load or because the values at the known address are unknown
            if len( intervals ) == 0 :
                fully_known = False
                singleton_value = False
                continue

            # We have at least some information, but we do not know if they are data to the load_set
            for val in intervals :
                minimum = int( val.attrib[ "min" ], base=16 )
                maximum = int( val.attrib[ "max" ], base=16 )
                mod = int( val.attrib.get( "mod", 0 ) )
                # rem = int(val.attrib["rem"], base=16)
                for i in range( minimum, maximum + 1, 2 ^ mod ) :
                    if i not in load_tag_set :
                        continue
                    values.add( i )
                    if singleton_value_data is None :
                        singleton_value_data = i

        if len( values ) == 0 :
            context_info[ context ] = ContextInfo( load_tag_set, area_set, False )
            continue

        # If the set of values analyzed in this context is better than the set of values in the load set
        if values < load_tag_set :
            context_info[ context ] = ContextInfo( values, area_set, True )
            partially_known = True
            if len( values ) > 1 :
                singleton_value = False
                fully_known = False
            elif singleton_value :
                if singleton_value_data is not None and singleton_value_data not in values :
                    singleton_value = False

        else :
            context_info[ context ] = ContextInfo( values, area_set, False )
            fully_known = False
            singleton_value = False

    return LoadAllContextInfo( load=load, context_info=context_info, fully_known=fully_known,
                               partially_known=partially_known,
                               singleton_value=singleton_value )

def get_extended_dfi_op( interface: InterfaceAiT, bin_path: str ) -> \
        Tuple[List[ ExtendedDFILoad ], List[ ExtendedDFIStore ] ] :
    dfi_loads = obj.extract_dfi_loads( Path(bin_path) )
    dfi_stores = obj.extract_dfi_stores( bin_path )
    icfg = interface.get_icfg()

    extended_dfi_stores: List[ ExtendedDFIStore ] = [ ]
    extended_dfi_loads: List[ ExtendedDFILoad ] = [ ]

    for node in icfg.nodes() :
        cfg = icfg.nodes[ node ][ ICFG._cfg_id ]
        r = cfg.range

        if r is None :
            continue

        for load in dfi_loads :
            if r.first_instruction <= load.start_add <= r.last_instruction :
                a3_fid = node
                a3_bbid_start = cfg.find_bb_with_add( load.start_add )
                if a3_bbid_start is None :
                    continue
                a3_bbid_checks = list( map( lambda c : cfg.find_bb_with_add( c.address ), load.check_data ) )
                a3_bbid_end = cfg.find_bb_with_add( load.end_add )
                extended_dfi_loads.append( ExtendedDFILoad( bin_info=load, a3_fid=a3_fid, a3_bbid_start=a3_bbid_start,
                                                            a3_bbid_checks=a3_bbid_checks, a3_bbid_end=a3_bbid_end ) )

        for store in dfi_stores :
            if r.first_instruction <= store.store_add <= r.last_instruction :
                a3_fid = node
                a3_bbid = cfg.find_bb_with_add( store.store_add )
                if a3_bbid is None :
                    continue
                extended_dfi_stores.append( ExtendedDFIStore( bin_info=store, a3_fid=a3_fid, a3_bbid=a3_bbid ) )

    return extended_dfi_loads, extended_dfi_stores

def handle_singleton_load( singleton_loads: List[LoadAllContextInfo], metadata_db: llvm.MetadataDB ):
    changed_met = [ ]
    for load in singleton_loads :
        singleton_tag = load.context_info.values().__iter__().__next__().value_set.__iter__().__next__()
        load_met = metadata_db.load_sets[ load.load.bin_info.load_id ]
        tag_met = None
        for i in range( 2, len( load_met.content ) ) :
            if load_met.content[ i ].content[ 0 ] == singleton_tag :
                tag_met = load_met.content[ i ]
                break

        if tag_met is None :
            raise Exception( "Unable to find tag_met" )

        changed_met.append( load_met )
        load_met.content = load_met.content[ :2 ] + [ tag_met ]

    return changed_met