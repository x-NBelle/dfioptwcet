# coding=utf-8

from configurations import config
from typing import List

from optDfi.experiment.Benchmark import Benchmark

def genOptionMakefile(benchmarks: List[Benchmark]):
    bench_name = " ".join( bench.tag for bench in benchmarks )

    return f"""
.PHONY: build-witness build-sota build-sandbox build-wordchain build-chaincut build-all clean-witness clean-dfi-analyzed clean-dfi-base clean-dfi-asm clean-dfi-exe clean-dfi-outputs clean-all clean gen-dfi-comet-outputs gen-dfi-comet-outputs gen_outputs
.PRECIOUS: %.annotated.ll %.analyzed.ll %.base.ll %.witness.S $(foreach t, sota sandboxElim wordChain chainCut, %.dfi.$(t).S) %.qemu.elf %.comet.elf %.qemu.output.txt %.comet.output.txt
    
CLANG = { config.DEFAULT_PROJECT_DIR / "llvm-project/build/bin/clang" }
LLVM_LINK = { config.DEFAULT_PROJECT_DIR / "llvm-project/build/bin/llvm-link" }
OPT   = { config.DEFAULT_PROJECT_DIR / "llvm-project/build/bin/opt" }
LLC   = { config.DEFAULT_PROJECT_DIR / "llvm-project/build/bin/llc" }
GCC   = /opt/riscv32i/bin/riscv32-unknown-elf-gcc
PHASAR = { config.DEFAULT_PHASAR_PATH }
QEMU   = /home/nbellec/research/thesis/qemu/build/qemu-system-riscv32
COMET = { config.DEFAULT_COMET_PATH }

ARCH_FLAGS  := { ' '.join( config.ARCH_FLAGS ) }
CLANG_FLAGS := { ' '.join( config.CLANG_FLAGS ) }
OPT_FLAGS   := $(ARCH_FLAGS) { ' '.join( config.OPT_FLAGS ) } -load { str(config.DEFAULT_PROJECT_DIR / 'llvm-project/build/lib/LLVMDFI.so') } 
LLC_FLAGS   := { ' '.join( config.LLC_FLAGS ) } $(ARCH_FLAGS)
GCC_FLAGS   := { ' '.join( config.GCC_FLAGS ) }    

BENCHS := {bench_name}

WITNESS_QEMU := $(foreach b,$(BENCHS), $(b)/$(b).witness.qemu.elf)
WITNESS_COMET := $(foreach b,$(BENCHS), $(b)/$(b).witness.comet.elf)

DFI_SOTA_QEMU := $(foreach b,$(BENCHS), $(b)/$(b).dfi.sota.qemu.elf)
DFI_SOTA_COMET := $(foreach b,$(BENCHS), $(b)/$(b).dfi.sota.comet.elf)
DFI_SANDBOX_QEMU := $(foreach b,$(BENCHS), $(b)/$(b).dfi.sandboxElim.qemu.elf)
DFI_SANDBOX_COMET := $(foreach b,$(BENCHS), $(b)/$(b).dfi.sandboxElim.comet.elf)
DFI_WORDCHAIN_QEMU := $(foreach b,$(BENCHS), $(b)/$(b).dfi.wordChain.qemu.elf)
DFI_WORDCHAIN_COMET := $(foreach b,$(BENCHS), $(b)/$(b).dfi.wordChain.comet.elf)
DFI_CHAINCUT_QEMU := $(foreach b,$(BENCHS), $(b)/$(b).dfi.chainCut.qemu.elf)
DFI_CHAINCUT_COMET := $(foreach b,$(BENCHS), $(b)/$(b).dfi.chainCut.comet.elf)

DFI_EXES := $(DFI_SOTA_QEMU) $(DFI_SOTA_COMET) $(DFI_SANDBOX_QEMU) $(DFI_SANDBOX_COMET) $(DFI_WORDCHAIN_QEMU) $(DFI_WORDCHAIN_COMET) $(DFI_CHAINCUT_QEMU) $(DFI_CHAINCUT_COMET)
DFI_ASM := $(foreach t, sota sandboxElim wordChain chainCut, $(foreach b,$(BENCHS), $(b)/$(b).dfi.$(t).S))
DFI_BASE := $(foreach b,$(BENCHS), $(b)/$(b).$(t).base.ll)
DFI_ANALYZED := $(foreach b,$(BENCHS), $(b)/$(b).$(t).analyzed.ll)

WITNESS_EXES := $(WITNESS_QEMU) $(WITNESS_COMET)
WITNESS_ASM := $(foreach b,$(BENCHS), $(b)/$(b).witness.S)
WITNESS_OUTPUTS := $(patsub %.elf, $(WITNESS_EXES), %.output.txt)

DFI_QEMU_OUTPUTS := $(foreach t, sota sandboxElim wordChain chainCut, $(foreach b,$(BENCHS), $(b)/$(b).dfi.$(t).qemu.output.txt))
DFI_COMET_OUTPUTS := $(foreach t, sota sandboxElim wordChain chainCut, $(foreach b,$(BENCHS), $(b)/$(b).dfi.$(t).comet.output.txt))
"""

def genBuilds():
    return f"""
build-witness: $(WITNESS_QEMU) $(WITNESS_COMET)
build-sota: $(DFI_SOTA_QEMU) $(DFI_SOTA_COMET)
build-sandbox: $(DFI_SANDBOX_QEMU) $(DFI_SANDBOX_COMET)
build-wordchain: $(DFI_WORDCHAIN_QEMU) $(DFI_WORDCHAIN_COMET)
build-chaincut: $(DFI_CHAINCUT_QEMU) $(DFI_CHAINCUT_COMET)

build-all: build-witness build-sota build-sandbox build-wordchain build-chaincut
"""

def genCleans():
    return """
clean-witness:
\trm -rf $(WITNESS_EXES) $(WITNESS_ASM) $(WITNESS_OUTPUTS)
    
clean-dfi-analyzed: clean-dfi-base
\trm -rf $(DFI_ANALYZED)
    
clean-dfi-base: clean-dfi-asm
\trm -rf $(DFI_BASE)

clean-dfi-asm: clean-dfi-exe
\trm -rf $(DFI_ASM)

clean-dfi-exe: clean-dfi-outputs
\trm -rf $(DFI_EXES)

clean-dfi-outputs:
\trm -rf $(DFI_QEMU_OUTPUTS) $(DFI_COMET_OUTPUTS)

clean-all:
\trm -rf $(BENCHS)

clean: clean-all
"""

def genOutputs():
    return """
%.qemu.output.txt: %.qemu.elf
\t$(QEMU) -cpu rv32 -smp 1 -m 128M -serial mon:stdio -bios none -nographic -machine virt -kernel $^ > $@ && grep "OK" $@

%.comet.output.txt: %.comet.elf
\t$(COMET) -f $^ > $@

gen-dfi-qemu-outputs: $(DFI_QEMU_OUTPUTS)

gen-dfi-comet-outputs: $(DFI_COMET_OUTPUTS)

gen_outputs: $(WITNESS_OUTPUTS) gen-dfi-qemu-outputs gen-dfi-comet-outputs
"""

def source_to_ll( benchmark: Benchmark ):
    ll_stem = { s: f"{s.stem}.ll" for s in benchmark.sources }
    return ll_stem

def genLinkedMakefile( benchmark: Benchmark ):
    ll_sources = source_to_ll( benchmark )
    includes = ' '.join( f'-I{include}/' for include in benchmark.includes )
    clang_flags = ' '.join( benchmark.clang_flags )

    if len(benchmark.sources) == 1:
        return f"""
{benchmark.tag}/{benchmark.tag}.linked.ll: {' '.join( [str(s) for s in benchmark.sources ] )}
\tmkdir -p {benchmark.tag}
\t$(CLANG) {includes} $^ {clang_flags} -S -emit-llvm -o $@
        """

    res = ""

    for source, ll in ll_sources.items() :
        res += f"""
{benchmark.tag}/{ll}: {source}
\tmkdir -p {benchmark.tag}
\t$(CLANG) {includes} {source} {clang_flags} -S -emit-llvm -o $@
"""

    ll_files = [ f"{benchmark.tag}/{ll}" for ll in ll_sources.values() ]

    res += f"""
{benchmark.tag}/{benchmark.tag}.linked.ll: { ' '.join(ll_files) }
\t$(LLVM_LINK) $^ -S -o $@
"""

    return res

def genAlignMakefile( benchmark: Benchmark ):
    opt_flag = "--O1" if benchmark.tag != "susan" else ""
    return f"""
{benchmark.tag}/{benchmark.tag}.aligned.ll: {benchmark.tag}/{benchmark.tag}.linked.ll
\t$(OPT) $(OPT_FLAGS) {opt_flag} --dfialignment -S $^ -o $@
"""

def genAnnotated():
    return f"""
%.annotated.ll: %.aligned.ll
\t$(OPT) $(OPT_FLAGS) --dfiannotation -S $^ -o $@
"""

def genAnalyzed():
    return f"""
%.analyzed.ll: %.annotated.ll
\t$(PHASAR) $^ $@
"""

def genOptimized(name: str, passes: List[str], previous_name:str):
    pass_cmd = " ".join([f"--{p}" for p in passes])
    return f"""
%.{name}.ll: %.{previous_name}.ll
\t$(OPT) $(OPT_FLAGS) {pass_cmd} -S $^ -o $@
"""

def genWitnessAsm():
    return f"""
%.witness.S: %.aligned.ll
\t$(LLC) $(LLC_FLAGS) $^ -o $@
"""

def genDFIAsm(name: str, passes: List[str], suffix_name: str):
    pass_cmd = " ".join( [ f"--{p}" for p in passes ] )
    return f"""
%.dfi.{name}.S: %.{suffix_name}.ll
\t$(LLC) $(LLC_FLAGS) --emit-dfi {pass_cmd} $^ -o $@
"""

def genQemuComet():
    return f"""
%.qemu.elf: %.S
\t$(GCC) $(GCC_FLAGS) $^ {config.DEFAULT_LINK_PATH}/crt.s {config.DEFAULT_LINK_PATH}/lib.c -T {config.DEFAULT_LINK_PATH}/linker-qemu-wcet.ld -o $@

%.comet.elf: %.S
\t$(GCC) $(GCC_FLAGS) $^ {config.DEFAULT_LINK_PATH}/crt.s {config.DEFAULT_LINK_PATH}/lib.c -T {config.DEFAULT_LINK_PATH}/linker-comet.ld -o $@
"""

classic_optimization_passes = [
    "dfioptequivalentclasses",
    "dfioptimizations",
    "dfiopttagcheck",
    "dfiintrinsicinstr"
]

def genCommonBenchMakefile( benchmarks: List[Benchmark] ):
    res = genOptionMakefile( benchmarks )

    for bench in benchmarks:
        res += genLinkedMakefile( bench )
        res += genAlignMakefile( bench )

    res += genAnnotated()
    res += genAnalyzed()

    res += genOptimized("base", classic_optimization_passes, "analyzed")
    res += genWitnessAsm()
    res += genDFIAsm( "sota", [], "base" )
    res += genDFIAsm( "sandboxElim", [ 'dfi-lschain-opts="sandbox-elim"' ], "base" )
    res += genDFIAsm( "wordChain", [ 'dfi-lschain-opts="sandbox-elim,word-chain"' ], "base" )
    res += genDFIAsm( "chainCut", [ 'dfi-lschain-opts="sandbox-elim,word-chain,chain-cut"' ], "base" )
    res += genQemuComet()
    res += genOutputs()
    res += genBuilds()
    res += genCleans()


    print(res)