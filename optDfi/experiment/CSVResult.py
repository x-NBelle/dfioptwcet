# coding=utf-8
from __future__ import annotations
from collections import defaultdict

from pathlib import Path
from typing import List, Any, Dict, Set, TypeVar, Generic

T = TypeVar('T')
D = TypeVar('D')

class CSVResult(Generic[T, D]):
    __slots__ = [ "bench", "characteristics", "data", "file" ]

    def __init__(self, characteristics: List[T], file: str | Path):
        self.bench: Set[str] = set()
        self.characteristics: List[T] = [ c for c in characteristics ]
        self.data: Dict[str, Dict[T, D]] = {}
        self.file = file

    def add_bench( self, bench: str ):
        self.bench.add( bench )
        self.data[ bench ] = defaultdict(str)

    def add_data( self, bench: str, charac: T, data: D ):
        if bench not in self.bench:
            raise Exception("Bench not found in CSVResult")
        self.data[bench][charac] = data

    def write_header( self ):
        with open( self.file, "w" ) as f:
            f.write( ",".join(["bench"] + self.characteristics)+"\n" )

    def write_bench_data( self, bench: str ):
        if bench not in self.bench:
            raise Exception("Bench not found in CSVResult")

        with open( self.file, "a" ) as f:
            san_bench = _sanitize_bench( bench )
            f.write(",".join( [ san_bench ] + [ str( self.data[ bench ][ c ] ) for c in self.characteristics ] ) + "\n" )

    def write_all_data( self ):
        with open( self.file, "w" ) as f:
            f.write( ",".join( ["bench"] + self.characteristics ) + "\n" )

            for bench in self.bench:
                san_bench = _sanitize_bench(bench)
                f.write( ",".join( [san_bench] + [ str(self.data[ bench ][ c ]) for c in self.characteristics ] ) + "\n" )


def _sanitize_bench( bench: str ) -> str:
    return bench.replace("_", "\\_")
