# coding=utf-8
from pathlib import Path
from typing import Optional, Union


def _path_or_None( p: Optional[ Path ], d: Optional[ Union[ str, Path ] ] ) -> Optional[ Path ] :
    return None if d is None or p is None else p / d


def _mkdir_optional( p: Optional[ Path ] ) :
    if p is not None :
        p.mkdir( parents=True, exist_ok=True )


def _extends_or_None( p: Optional[ Path ], d: Optional[ Union[ str, Path ] ] ) -> Optional[ Path ] :
    path = _path_or_None( p, d )
    _mkdir_optional( path )
    return path


class FileSystem( object ) :
    """A class that contains the current experiment directory for the compilation as well as functions to
    have a single point of truth for the name of the files we use"""
    base_dir: Path
    bench_name: Optional[ str ]
    it_num: Optional[ int ]

    def __init__( self, base: Path, bench_name: Optional[ str ] = None, it_num: Optional[ int ] = None ) :
        self.base_dir = base.absolute()

        self.bench_name = bench_name
        self.it_num = it_num

        self.command_file = self.base_dir / "commands.txt"

        self.base_dir.mkdir(parents=True, exist_ok=True)

        if self.it_num is not None and self.bench_name is None :
            raise Exception( "Generated ExpDirContext with iteration number but without benchmark name" )

        self.bench_path = _extends_or_None( self.base_dir, self.bench_name )

        # Inside {bench}
        self.common_path = _extends_or_None( self.bench_path, "common/" )
        self.witness_path = _extends_or_None( self.bench_path, "witness/" )
        self.dfi_path = _extends_or_None( self.bench_path, "dfi/" )

        # Inside {bench}/common
        self.common_llvm = _extends_or_None( self.common_path, "llvm/" )
        self.common_aiT = _extends_or_None( self.common_path, "aiT/" )

        # Inside {bench}/common/llvm
        self.linked_ll = _path_or_None( self.common_llvm, f"{bench_name}.linked.ll" )
        self.aligned_ll = _path_or_None( self.common_llvm, f"{bench_name}.aligned.ll" )

        # Inside {bench}/common/aiT
        self.base_ais = _path_or_None( self.common_aiT, "base.ais" )

        # Inside {bench}/witness
        self.witness_bin = _extends_or_None( self.witness_path, "bin/" )
        self.witness_aiT = _extends_or_None( self.witness_path, "aiT/" )
        self.witness_asm = _path_or_None( self.witness_path, f"{bench_name}.witness.S" )
        self.witness_qemu_output = _path_or_None( self.witness_path, f"{bench_name}_output.qemu.txt" )

        # Inside {bench}/witness/bin
        self.witness_comet_exe_path = _path_or_None( self.witness_bin, f"{bench_name}.witness.comet.elf" )
        self.witness_qemu_exe_path = _path_or_None( self.witness_bin, f"{bench_name}.witness.qemu.elf" )

        # Inside {bench}/witness/aiT
        self.witness_ais = _path_or_None( self.witness_aiT, f"{bench_name}.witness.ais" )
        self.witness_txt = _path_or_None( self.witness_aiT, f"{bench_name}.witness.txt" )
        self.witness_xml = _path_or_None( self.witness_aiT, f"{bench_name}.witness.xml" )
        self.witness_apx = _path_or_None( self.witness_aiT, f"{bench_name}.witness.apx" )

        # Inside {bench}/dfi
        self.iteration_path = _extends_or_None( self.dfi_path, f"{self.it_num}/" if self.it_num is not None else None )
        self.annotated_ll = _path_or_None( self.dfi_path, f"{bench_name}.annotated.ll" )
        self.analysed_ll = _path_or_None( self.dfi_path, f"{bench_name}.analysed.ll" )

        # Inside {bench}/dfi/{it_num}
        self.iteration_bin = _extends_or_None( self.iteration_path, "bin/" )
        self.iteration_aiT = _extends_or_None( self.iteration_path, "aiT/" )
        self.iteration_ilp = _extends_or_None( self.iteration_path, "ilp/" )
        self.iteration_asm = _path_or_None( self.iteration_path, f"{bench_name}_{it_num}.dfi.S" )
        self.iteration_optimized_ll = _path_or_None( self.iteration_path, f"{bench_name}_{it_num}.optimized.ll" )
        self.iteration_qemu_output = _path_or_None( self.iteration_path, f"{bench_name}_{it_num}_output.qemu.txt" )

        # Inside {bench}/dfi/{it_num}/ilp
        self.it_ilp_log = _path_or_None( self.iteration_ilp, f"{bench_name}_{it_num}.ilp_stat.log" )
        self.it_problem_log = _extends_or_None( self.iteration_ilp, f"{bench_name}_{it_num}.ilp_problems/" )

        # Inside {bench}/dfi/{it_num}/bin
        self.it_bin_qemu = _path_or_None( self.iteration_bin, f"{bench_name}_{it_num}.dfi.qemu.elf" )
        self.it_bin_comet = _path_or_None( self.iteration_bin, f"{bench_name}_{it_num}.dfi.comet.elf" )

        # Inside {bench}/dfi/{it_num}/aiT
        self.it_aiT_ais = _path_or_None( self.iteration_aiT, f"{bench_name}_{it_num}.dfi.ais" )
        self.it_aiT_apx = _path_or_None( self.iteration_aiT, f"{bench_name}_{it_num}.dfi.apx" )
        self.it_aiT_txt = _path_or_None( self.iteration_aiT, f"{bench_name}_{it_num}.dfi.txt" )
        self.it_aiT_xml = _path_or_None( self.iteration_aiT, f"{bench_name}_{it_num}.dfi.xml" )

