# coding=utf-8
from typing import List, Tuple, Optional

from optDfi.interface.a3.misc import BBData
from optDfi.interface.a3.a3 import InterfaceAiT
from optDfi.interface.objdump import ObjdumpInstruction, ObjdumpDFILabel


class NoInstructionException( Exception ) :
    pass


class WrongAddressPadding( Exception ) :
    pass


def search_in_instructions( instructions: List[ ObjdumpInstruction ], address: int ) -> int :
    """
    Returns the index of the instruction at the given address in the list of objdump generated instructions
    """
    # RiscV dependent
    if len( instructions ) == 0 :
        raise NoInstructionException()
    if address % 4 != 0 :
        raise WrongAddressPadding()

    first_add = int( instructions[ 0 ].address )
    return (address - first_add) // 4


def get_index_inst_between( instructions: List[ ObjdumpInstruction ],
                            address_start: int, address_end: int ) -> Tuple[ int, int ] :
    """
    Returns the index of beginning and end of the objdump generated instructions that are researched
    """

    return search_in_instructions( instructions, address_start ), search_in_instructions( instructions, address_end )


def find_start_bb_of_label( xml_a3: InterfaceAiT, label: ObjdumpDFILabel ) -> Optional[ BBData ] :
    for r in xml_a3.icfg.nodes().values() :
        if r.get( "range", None ) is None :
            continue
        if r[ "range" ].first_instruction <= label.address <= r[ "range" ].last_instruction :
            # Found the routine, now searching for the basic block
            for bb in r[ "cfg" ].nodes().values() :
                if bb.get( "range", None ) is None :
                    continue
                if bb[ " range" ].first_instruction <= label.address <= bb[ "range" ].last_instruction :
                    # Found the basic block
                    return bb[ "range" ]

    return None

def cost_inst( inst: ObjdumpInstruction ):
    one_cycle_inst = [
        "add", "sub", "slt", "sltu", "xor", "or", "and", "lui", "auipc",
        "sll", "srl", "sra"
    ]
    two_cycle_inst = [
        "lb", "lh", "lw", "lbu", "lhu", "sb", "sh", "sw",
        "csrrw", "csrrs", "csrrc"
    ]
    three_cycle_inst = [
        "beq", "bne", "blt", "bge", "bltu", "bgeu", "jal", "jalr",
        "ecall", "ebreak", "fence"
    ]
    other_inst =  [
        "mul", "mulh", "mulhsu", "mulhu", "div", "divu", "rem", "remu"
    ]

    for i in one_cycle_inst:
        if i in inst.name:
            return 1

    for i in two_cycle_inst:
        if i in inst.name:
            return 2

    for i in three_cycle_inst:
        if i in inst.name:
            return 3

    for i in other_inst:
        if i in inst.name:
            return 34

    raise Exception( "Unknown instruction : unable to find the cost" )

def compute_overcost_dfi_label( xml_a3: InterfaceAiT, label: ObjdumpDFILabel,
                                instructions: List[ ObjdumpInstruction ] ):
    adds: Optional[ BBData ] = find_start_bb_of_label( xml_a3, label )

    if adds is None:
        raise Exception( "Basic block not found" )

    if ".b" in label.label:
        # Begin label
        start, end = get_index_inst_between( instructions, adds.first_instruction, label.address )
        cost = 0
        for i in range(start, end):
            cost += cost_inst( instructions[ i ] )

        return cost

    elif ".e" in label.label:
        # End label
        start, end = get_index_inst_between( instructions, label.address, adds.last_instruction )
        cost = 0
        for i in range( start+1, end+1 ) :
            cost += cost_inst( instructions[ i ] )

        return cost

    else:
        raise Exception( "Unknown dfi label" )