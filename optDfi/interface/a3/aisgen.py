# coding=utf-8
from pathlib import Path
import subprocess as sp
from typing import Optional

from configurations import config


def gen_apx_files( executable: Path, ais_file: Path, output_file: Path, xml_file: Path, txt_file: Path ) :
    template: Optional[ str ] = None

    with open( config.APX_TEMPLATE_PATH, "r" ) as f :
        template = "".join( f.readlines() )

    apx = template.format(
        executable=executable,
        xml_report=xml_file,
        ais=ais_file,
        txt_file=txt_file
    )

    with open( output_file, "w" ) as f :
        f.write( apx )

def ais_append_breaks( ais_file: Path, executable: Path ) :
    with open( ais_file, "a" ) as f :
        f.write( "try {\n" )

    objdump = "riscv32-unknown-elf-objdump"
    cmd_line = f"{objdump} -d {executable} | grep 'ebreak' | awk '{{ sub(\":\", \"\", $1); print \"instruction 0x\" $1 \"-0x4 condition always taken;\" }}' >> {ais_file}"

    sp.run( cmd_line, shell=True, stdout=sp.PIPE, text=True, encoding='utf-8' )

    with open( ais_file, "a" ) as f :
        f.write( "}\n" )
