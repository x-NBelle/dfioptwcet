# coding=utf-8

from __future__ import annotations
import pathlib
import xml.etree.ElementTree as ET
from typing import Iterable, List, Optional

from optDfi.interface.a3.ICFG import ICFG, CFG
from optDfi.interface.a3.WCETPath import WCETPath, WCETNode
from optDfi.interface.a3.exceptions import FailedHypothesisException
from optDfi.interface.a3.misc import NS, XPATH_VALUE_INSTRUCTION, XPATH_ROUTINES, BBData, XPATH_WCET_CONTEXT_FORMAT, \
    XPATH_WCET_START_FORMAT, XPATH_WCET_END_FORMAT, XPATH_WCET_PATH, XPATH_WCET_ROUTINE_FORMAT


class InterfaceAiT( object ) :
    def __init__( self, pathname: str ) :
        """
        Constructor of InterfaceAiT

        :param pathname: Path to the xml result_file of AiT results
        :type pathname: str
        """
        self.icfg: Optional[ ICFG ] = None
        self.wcet_path: Optional[ WCETPath ] = None
        self.xml_root = ET.parse( pathlib.Path( pathname ) ).getroot()

    @staticmethod
    def _gen_xpath( xpath_el: Iterable[ str ] ) -> str :
        """
        Generate a xpath in an AiT XML report with the correct tag
        at each part

        :param xpath_el: iterable of part of the xpath to fusion
        :type xpath_el: iterable of str
        :return: the xpath annotated with the right tags
        """
        xpath = map( lambda el : el if "." in el or "/" in el or ":" in el else "a3:" + el, xpath_el )
        return "/".join( xpath )

    def _findall_xpath( self, xpath_el: Iterable[ str ] ) -> List[ ET.Element ] :
        """
        Execute a xpath findall request on the root of the xml with correct namespace

        :param xpath_el: list of xpath part to aggregate to form a path
        :return: Element list
        """
        return self.xml_root.findall( self._gen_xpath( xpath_el ), NS )

    def _find_xpath( self, xpath_el: Iterable[ str ] ) -> Optional[ ET.Element ] :
        """
        Execute a xpath find request on the root of the xml with correct
        namespace

        :param xpath_el: list of xpath part to aggregate to form a path
        :return: Optional[Element]
        """
        return self.xml_root.find( self._gen_xpath( xpath_el ), NS )

    def _get_value_instruction( self, address: int ) -> List[ ET.Element ] :
        return self._findall_xpath( [ XPATH_VALUE_INSTRUCTION + "[@address='{add}']".format( add=hex( address ) ) ] )

    def _get_real_block_with_type( self, type_str: str ) -> List[ ET.Element ] :
        """
        Return a xpath to access a basic block with specific type that contains at least one instruction

        :param type_str: the type to search for
        :type type_str: str
        :return: list of blocks with the appropriate type
        :rtype: List[Element]
        """

        return self._findall_xpath(
            [ XPATH_ROUTINES, "block[@type='{type}']".format( type=type_str ),
              "first_instruction", ".." ]
        )

    def _get_routine_from_id( self, id_str: str ) -> Optional[ ET.Element ] :
        """
        Recovers the routine XML Element associated to `id_str` or
        which block with id equals to `id_str` is in

        :param id_str: id to match to find the correct block / routine
        :type id_str: str
        :return: A Element routine that has the id or contains a block with the id
        :rtype: Optional[Element]
        """
        routine = self._find_xpath( [ XPATH_ROUTINES + "[@id='{id}']".format( id=id_str ) ] )
        if routine is not None :
            return routine

        routine_from_block = self._find_xpath(
            [ XPATH_ROUTINES, "block[@id='{id}']".format( id=id_str ), ".." ] )

        return routine_from_block

    def _handle_normal_block( self, block: ET.Element, cfg: CFG ) :
        """
        Analyze the element block known as a normal block

        :param block: The Element block to analyze
        :param cfg: The cfg of the function containing the block
        :return: None
        """
        first_instruction = int( block.find( "./a3:first_instruction", NS ).text, base=16 )
        last_instruction = int( block.find( "./a3:last_instruction", NS ).text, base=16 )
        bbid = block.attrib[ "id" ]
        bbdata = BBData( first_instruction=first_instruction,
                         last_instruction=last_instruction )
        cfg.add_bb( bbid=bbid, attrib=block.attrib, bbdata=bbdata, bbname="N_" + bbid )

        successors = block.findall( "./a3:successor", NS )

        for succ_node in successors :
            cfg.add_edge( bbid, succ_node.text )

    def _handle_call_block( self, block: ET.Element, caller_id: str, cfg: CFG ) :
        """
        Analyze the element block known as a call block

        :param block: The Element block to analyze
        :param caller_id: The id of the routine in which the call block is
        :param cfg: The cfg of the function containing the block
        :return: None
        """
        bbid = block.attrib[ "id" ]
        cfg.add_bb( bbid, attrib=block.attrib, bbname="C_" + bbid )
        successors = block.findall( "./a3:successor", NS )

        for succ_node in successors :
            succ_id = succ_node.text
            succ_routine = self._get_routine_from_id( succ_id )

            assert succ_routine is not None

            callee_id = succ_routine.attrib[ "id" ]
            if caller_id == callee_id :
                # Case we are in a loop or the fallthrough edge
                cfg.add_edge( bbid, succ_id )
            else :
                # Case we have a real call
                self.icfg.add_call( caller_id=caller_id, callee_id=callee_id, bb_id=bbid )

    def _handle_start_return_block( self, block: ET.Element, cfg: CFG ) :
        """
        Analyze the element block known as a return block

        :param block: The Element block to analyze
        :param cfg: The cfg of the function containing the block
        :return: None
        """
        # TODO: Check the successor is indeed in the function
        bb_id = block.attrib[ "id" ]
        cfg.add_bb( bbid=bb_id, attrib=block.attrib, bbname="SR_" + bb_id )
        successors = block.findall( "./a3:successor", NS )

        for succ_node in successors :
            cfg.add_edge( bb_id, succ_node.text )

    def _handle_end_block( self, block: ET.Element, cfg: CFG ) :
        """
        Analyze the element block known as a end block

        :param block: The Element block to analyze
        :param cfg: The cfg of the function containing the block
        :return: None
        """
        bbid = block.attrib[ "id" ]
        cfg.add_bb( bbid, block.attrib, "E_" + bbid )

        # routine_node = lc1._get_routine_from_id( bbid )
        # if routine_node.attrib.get( "loop", None ) is not None :
        #     # This is the end of a loop
        #     successors = block.findall( "./a3:successor", NS )
        #
        #     for succ_node in successors :
        #         cfg.add_edge( block.attrib[ "id" ], succ_node.text )

    def _icfg_construction( self ) :
        """
        Analyze the XML to construct the icfg of the program

        :return: None
        """

        for routine_id in self.icfg.nodes() :
            routine = self._get_routine_from_id( routine_id )

            # Recover blocks
            blocks = self._findall_xpath( [
                XPATH_ROUTINES + "[@id='{id}']".format( id=routine_id ),
                "block" ] )

            cfg = self.icfg.getCFG( fid=routine_id )
            for block in blocks :
                type_block = block.attrib[ "type" ]
                if type_block == "normal" :
                    self._handle_normal_block( block, cfg )
                elif type_block == "call" :
                    self._handle_call_block( block, routine_id, cfg )
                elif type_block == "return" :
                    self._handle_start_return_block( block, cfg )
                elif type_block == "start" :
                    self._handle_start_return_block( block, cfg )
                elif type_block == "end" :
                    self._handle_end_block( block, cfg )
                    pass
                elif type_block == "external" :
                    pass
                else :
                    assert False

    def _gen_initial_cfg( self ) :
        """
        Generates one cfg in the ICFG for each real routine (not extracted) in the XML

        :return: None
        """

        # Recover the routines in the xml
        routines = self._findall_xpath( [ XPATH_ROUTINES ] )

        # Generating the CFGs
        for f in filter( lambda r : "UnresolvedTarget_" not in r.attrib[ "name" ], routines ) :
            self.icfg.add_function( fid=f.attrib[ "id" ],
                                    fname=f.attrib[ "name" ],
                                    attrib=f.attrib )

    def _analyze_routine_wcet_path( self, routine_id: str, context_id: str ) :
        """
        Analyze the wcet path of a routine and add the constructed graph to the wcet_path graph

        :param routine_id:
        :param context_id:
        :return:
        """
        context_node = self._find_xpath( [ XPATH_WCET_CONTEXT_FORMAT.format(
            routine=routine_id,
            context=context_id
        ) ] )

        # Place the annotation on the starting and ending nodes
        start_node = context_node.find( "./a3:wcet_start", NS )
        start_block = start_node.attrib[ "block" ]
        start_id = WCETNode( routine_id=routine_id, bb_id=start_block, context_id=context_id )
        end_node = context_node.find( "./a3:wcet_end", NS )
        end_block = end_node.attrib[ "block" ]
        end_id = WCETNode( routine_id=routine_id, bb_id=end_block, context_id=context_id )

        # Check that it did not already analyzed this routine
        if self.wcet_path.nodes.get( start_id, None ) is not None :
            return

        self.wcet_path.add_node( start_id, start_routine=True )
        self.wcet_path.add_node( end_id, end_routine=True )

        # Analyze the edges
        for edge in context_node.findall( "a3:wcet_edge", NS ) :
            source_block = edge.attrib[ "source_block" ]
            source_context = edge.attrib[ "source_context" ]
            target_block = edge.attrib[ "target_block" ]
            target_context = edge.attrib[ "target_context" ]
            count = edge.attrib.get( "count", None )
            cycles = edge.attrib.get( "cycles", None )

            source_wcet_id = WCETNode( routine_id=routine_id, bb_id=source_block, context_id=source_context )
            target_wcet_id = WCETNode( routine_id=routine_id, bb_id=target_block, context_id=target_context )

            self.wcet_path.add_edge( source_wcet_id, target_wcet_id )

            if count is not None :
                self.wcet_path[ source_wcet_id ][ target_wcet_id ][ "count" ] = int( count )
            if cycles is not None :
                self.wcet_path[ source_wcet_id ][ target_wcet_id ][ "cycles" ] = int( cycles )

        # Analyze the calls
        for call in context_node.findall( "a3:wcet_edge_call", NS ) :
            source_block = call.attrib[ "source_block" ]
            source_context = call.attrib[ "source_context" ]
            target_routine = call.attrib[ "target_routine" ]
            target_context = call.attrib[ "target_context" ]
            count = call.attrib.get( "count", None )
            cycles = call.attrib.get( "cycles", None )

            self._analyze_routine_wcet_path( target_routine, target_context )

            target_start_node = self._find_xpath( [ XPATH_WCET_START_FORMAT.format(
                routine=target_routine,
                context=target_context
            ) ] )
            target_block = target_start_node.attrib[ "block" ]

            source_wcet_id = WCETNode( routine_id=routine_id, bb_id=source_block, context_id=source_context )
            target_wcet_id = WCETNode( routine_id=target_routine, bb_id=target_block, context_id=target_context )

            self.wcet_path.add_edge( source_wcet_id, target_wcet_id )

            if count is not None :
                self.wcet_path[ source_wcet_id ][ target_wcet_id ][ "count" ] = int( count )
            if cycles is not None :
                self.wcet_path[ source_wcet_id ][ target_wcet_id ][ "cycles" ] = int( cycles )

        # Analyze the returns
        for ret in context_node.findall( "a3:wcet_edge_return", NS ) :
            source_routine = ret.attrib[ "source_routine" ]
            source_context = ret.attrib[ "source_context" ]
            target_block = ret.attrib[ "target_block" ]
            target_context = ret.attrib[ "target_context" ]
            count = ret.attrib.get( "count", None )
            cycles = ret.attrib.get( "cycles", None )

            source_end_node = self._find_xpath( [ XPATH_WCET_END_FORMAT.format(
                routine=source_routine,
                context=source_context
            ) ] )

            end_block = source_end_node.attrib[ "block" ]

            source_wcet_id = WCETNode( routine_id=source_routine, bb_id=end_block, context_id=source_context )
            target_wcet_id = WCETNode( routine_id=routine_id, bb_id=target_block, context_id=target_context )

            self.wcet_path.add_edge( source_wcet_id, target_wcet_id )

            if count is not None :
                self.wcet_path[ source_wcet_id ][ target_wcet_id ][ "count" ] = int( count )
            if cycles is not None :
                self.wcet_path[ source_wcet_id ][ target_wcet_id ][ "cycles" ] = int( cycles )

    def test_hypothesis( self ) -> bool :
        """
        Test if the hypotheses hold in the loaded xml (especially to
        debug the reconstruction algorithm)

        :return: True if the hypotheses hold, False otherwise
        """
        for type_str in [ "start", "end", "call", "return" ] :
            if len( self._get_real_block_with_type( type_str ) ) != 0 :
                raise FailedHypothesisException( "no instruction in block of type '{type}'".format( type=type_str ) )

        # NB: Very costly for the tests so avoid it
        for normal_block in self._findall_xpath( [ XPATH_ROUTINES, "block[@type='normal']" ] ) :
            base_routine = self._find_xpath( [ XPATH_ROUTINES,
                                               "block[@id='{id}']".format( id=normal_block.attrib[ "id" ] ),
                                               ".." ] )
            if base_routine is None :
                raise FailedHypothesisException(
                    "Unable to find a routine for a block '{id}'".format( id=normal_block.attrib[ "id" ] ) )

            for successor in normal_block.findall( "./a3:successor", NS ) :
                successor_id = successor.text
                successor_routine = self._find_xpath(
                    [ XPATH_ROUTINES, "block[@id='{id}']".format( id=successor_id ), ".." ] )

                if successor_routine is None :
                    raise FailedHypothesisException( "Found multiple successor routines" )

                if successor_routine.attrib[ "id" ] != base_routine.attrib[ "id" ] :
                    raise FailedHypothesisException(
                        "Normal block with outside successor : '{id}'".format( id=normal_block.attrib[ "id" ] ) )

        return True

    def get_icfg( self ) -> ICFG :
        """
        Recovers the Inter-procedural control-flow graph from the xml
        """
        if self.icfg is not None :
            return self.icfg

        self.icfg = ICFG()
        self._gen_initial_cfg()
        self._icfg_construction()

        return self.icfg

    def get_wcet_path( self ) -> WCETPath :
        """
        Recover the wcet path from the xml

        :return:
        """
        if self.wcet_path is not None :
            return self.wcet_path

        self.wcet_path = WCETPath( self.icfg )

        wcet_entry_node = self._find_xpath( [ XPATH_WCET_PATH, "wcet_entry" ] )
        starting_routine = wcet_entry_node.attrib[ "routine" ]
        starting_context_nodes = self._findall_xpath( [
            XPATH_WCET_ROUTINE_FORMAT.format( routine=starting_routine ),
            "wcet_context"
        ] )

        if len( starting_context_nodes ) != 1 :
            raise Exception( "More than one starting context" )

        starting_context = starting_context_nodes[ 0 ].attrib[ "context" ]

        self._analyze_routine_wcet_path( starting_routine, starting_context )

        # For placing the start and end annotation after the creation of the graph
        start_node = starting_context_nodes[ 0 ].find( "./a3:wcet_start", NS )
        start_block = start_node.attrib[ "block" ]
        end_node = starting_context_nodes[ 0 ].find( "./a3:wcet_end", NS )
        end_block = end_node.attrib[ "block" ]
        start_id = WCETNode( routine_id=starting_routine, bb_id=start_block, context_id=starting_context )
        end_id = WCETNode( routine_id=starting_routine, bb_id=end_block, context_id=starting_context )

        self.wcet_path.start_id = start_id
        self.wcet_path.end_id = end_id
        return self.wcet_path
