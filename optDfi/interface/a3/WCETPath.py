# coding=utf-8
from __future__ import annotations
from typing import NamedTuple, Optional, Any
import networkx as nx

from optDfi.interface.a3.ICFG import ICFG
from optDfi.misc.graph_representation import print_graph


class WCETNode( NamedTuple ) :
    routine_id: str
    bb_id: str
    context_id: str

    def __repr__( self ) :
        return "{routine_id}|{bb_id}|{context_id}".format(
            routine_id=self.routine_id,
            bb_id=self.bb_id,
            context_id=self.context_id
        )


class WCETPath( nx.DiGraph ) :
    """
    Class to represent the WCET path of a program
    """

    def __init__( self, icfg: ICFG, *args, **kwargs ) :
        super().__init__( *args, **kwargs )
        self.icfg = icfg
        self.start_id: Optional[ WCETNode ] = None
        self.end_id: Optional[ WCETNode ] = None

    @staticmethod
    def _collapse_id( label: WCETNode ) -> WCETNode :
        return WCETNode( routine_id=label.routine_id, bb_id=label.bb_id, context_id="" )

    def get_count( self, source: WCETNode, target: WCETNode ) -> Optional[ int ] :
        if (source, target) in self.edges() :
            return self.edges[ source ][ target ].get( "count", None )
        else :
            return None

    def get_cycles( self, source: WCETNode, target: WCETNode ) -> Optional[ int ] :
        if (source, target) in self.edges() :
            return self.edges[ source ][ target ].get( "cycles", None )
        else :
            return None

    def _middle_node_bypassing( self, graph: nx.DiGraph, node: Any ) :
        if node not in graph :
            raise Exception( "Impossible to bypass node not in the graph" )

        entry_nodes = [ s for s, _ in graph.in_edges( node ) if s != node ]
        exit_nodes = [ t for t in graph.neighbors( node ) if t != node ]

        for s in entry_nodes :
            for t in exit_nodes :
                graph.add_edge( s, t )

        graph.remove_node( node )

    def collapse_wcet_path( self ) -> WCETPath :
        collapsed = WCETPath( self.icfg )

        cycles = { }
        count = { }

        for node in self.nodes :
            collapsed.add_node( self._collapse_id( node ), **self.nodes[ node ] )

        for s, t in self.edges :
            new_s = self._collapse_id( s )
            new_t = self._collapse_id( t )

            if "cycles" in self.edges[ s, t ] :
                cycles[ (new_s, new_t) ] = cycles.get( (new_s, new_t), 0 ) + self.edges[ s, t ][ "cycles" ]
            if "count" in self.edges[ s, t ] :
                count[ (new_s, new_t) ] = count.get( (new_s, new_t), 0 ) + self.edges[ s, t ][ "count" ]
            collapsed.add_edge( new_s, new_t, **self.edges[ s, t ] )

        for e, c in cycles.items() :
            collapsed.edges[ e[ 0 ], e[ 1 ] ][ "cycles" ] = c
        for e, c in count.items() :
            collapsed.edges[ e[ 0 ], e[ 1 ] ][ "count" ] = c

        collapsed.start_id = self.start_id
        collapsed.end_id = self.end_id
        return collapsed

    def get_non_zero_count_wcet_path( self ) -> WCETPath :
        new_wcet_path = WCETPath( self.icfg )

        for s, t in self.edges :
            if s is self.start_id or s is self.end_id :
                continue
            if self[ s ][ t ][ "count" ] != 0 :
                new_wcet_path.add_edge( s, t, **(self.edges[ s, t ]) )

        for s in new_wcet_path.nodes :
            new_wcet_path.add_node( s, **self.nodes[ s ] )

        # non_timing_edge = list(
        #     filter( lambda e : "cycles" not in new_wcet_path.edges[ e ] and e[0] == lc1.start_id and e[1] == lc1.end_id,
        #             new_wcet_path.edges() )
        # )
        # non_timing_node = { e[ 1 ] for e in non_timing_edge }
        #
        # for node in non_timing_node :
        #     lc1._middle_node_bypassing( new_wcet_path, node )

        new_wcet_path.start_id = self.start_id
        new_wcet_path.end_id = self.end_id
        return new_wcet_path

    def _id_to_fname( self, fid: str ) :
        if self.icfg is None :
            return fid

        if fid in self.icfg.id_name_map :
            return self.icfg.id_name_map[ fid ]
        else :
            return fid

    def _transform_block_name( self, node: WCETNode ) :
        return WCETNode( routine_id=self._id_to_fname( node.routine_id ), bb_id=node.bb_id, context_id=node.context_id )

    def print( self, filename="wcet_graph" ) :
        graph = nx.DiGraph()

        graph.add_edges_from(
            map( lambda e : (self._transform_block_name( e[ 0 ] ), self._transform_block_name( e[ 1 ] )),
                 self.edges() ) )

        print_graph( graph, filename )
