# coding=utf-8

from __future__ import annotations

import re
import subprocess as sp
from typing import NamedTuple, List, Union, Dict


class LLVMMetadata( object ) :
    __slots__ = ["name", "content"]
    name: str
    content: List[ Union[ str, int, LLVMMetadata ] ]

    def __init__(self, name, content):
        self.name = name
        self.content = content

    def __repr__( self ) :
        res = "!{name} = ".format( name=self.name )
        inside = [ ]
        for data in self.content :
            if type( data ) == str :
                inside.append( "!\"{s}\"".format( s=data ) )
            elif type( data ) == int :
                inside.append( "i32 {i}".format( i=data ) )
            elif type( data ) == LLVMMetadata :
                inside.append( "!{name}".format( name=data.name ) )

        inside_str = ", ".join( inside )
        res += "!{" + inside_str + "}"
        return res

class MetadataDB( object ):
    name_dict: Dict[ str, LLVMMetadata ]
    load_sets: Dict[ int, LLVMMetadata ]

    def __init__(self):
        self.name_dict = {}
        self.load_sets = {}


def modify_metadata( filename: str, metadata: LLVMMetadata ) :
    cmd = "sed -i 's/!{name} = .*/{repr}/'  {file}".format( name=metadata.name, repr=repr( metadata ), file=filename )

    sp.run( cmd, shell=True )


def modify_metadata_batch( in_filename: str, out_filename: str, metadatas: List[ LLVMMetadata ] ) :
    same_file = in_filename == out_filename
    cmd_base = "sed -i" if same_file else "sed"
    cmd_part_list = [ cmd_base ]
    for metadata in metadatas :
        cmd_part_list.append( "-e 's/!{name} = .*/{repr}/'".format( name=metadata.name, repr=repr( metadata ) ) )

    cmd_part_list.append( in_filename )
    if not same_file:
        cmd_part_list.append( ">" )
        cmd_part_list.append( out_filename )

    cmd = " ".join( cmd_part_list )

    sp.run( cmd, shell=True )


def retrieve_llvm_metadatas( filename: str ) -> MetadataDB:
    metadata_db = MetadataDB()
    waiting_metadatas = [ ]

    part_re = re.compile( ", !([0-9]*)" )
    dfi_load_set_re = re.compile( "^!([0-9]*) = !{!([0-9]*), !\"dfi_load_set\"((?:, ![0-9]*)*)}$" )
    dfi_load_id_re = re.compile( "^!([0-9]*) = !{i32 ([0-9]*), !\"dfi_load_id\"}$" )
    dfi_store_tag_re = re.compile( "^!([0-9]*) = !{i32 ([0-9]*), !\"dfi_store_tag\"}$" )

    with open( filename, "r" ) as f :
        for l in f :
            set_match = dfi_load_set_re.match( l )
            id_match = dfi_load_id_re.match( l )
            store_tag_match = dfi_store_tag_re.match( l )

            if set_match is not None :
                waiting_metadatas.append( set_match )
                continue

            if id_match is not None :
                metadata_db.name_dict[ id_match.group( 1 ) ] = \
                    LLVMMetadata( name=id_match.group( 1 ), content=[ int( id_match.group( 2 ) ), "dfi_load_id" ] )
                continue

            if store_tag_match is not None :
                metadata_db.name_dict[ store_tag_match.group( 1 ) ] = \
                    LLVMMetadata( name=store_tag_match.group( 1 ),
                                  content=[ int( store_tag_match.group( 2 ) ), "dfi_store_tag" ] )
                continue

    for waiting in waiting_metadatas :
        parts = [metadata_db.name_dict[ waiting.group( 2 ) ], "dfi_load_set" ]
        for name in part_re.findall( waiting.group( 3 ) ) :
            parts.append( metadata_db.name_dict[ name ] )
        metadata_db.name_dict[ waiting.group( 1 ) ] = LLVMMetadata( name=waiting.group( 1 ), content=parts )
        metadata_db.load_sets[ parts[0].content[0] ] = metadata_db.name_dict[ waiting.group( 1 ) ]

    return metadata_db

# retrieve_llvm_metadatas( "/home/nbellec/research/thesis/DFI/tests/build/llvm/tacle_gsm_enc/dfi/tacle_gsm_enc.intrinsic.ll" )