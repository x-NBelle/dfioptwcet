# coding=utf-8
from __future__ import annotations

import logging
from dataclasses import dataclass
from itertools import chain
from typing import List, Dict, Iterator, Set, Iterable, Optional, NoReturn, Tuple, Union

import pipe

from optDfi.optimizations.ilp_opt.ilp_problem.context import Tag, LoadId, LoadContexts
from optDfi.optimizations.ilp_opt.ilp_problem.equivalence_map import EqClassMap


@dataclass
class ContextPath( object ) :
    load_contexts: Dict[ LoadId, LoadContexts ]

    def __init__( self, load_contexts: Union[ Iterable[ LoadContexts ], Dict[ LoadId, LoadContexts ] ] = () ) :
        if isinstance( load_contexts, dict ) :
            self.load_contexts = dict( load_contexts )

        else :
            self.load_contexts = { }

            for load in load_contexts :
                if load.loadid in self.load_contexts :
                    logging.warning( f"Inserting 2 times the key {load.loadid} in a ContextPath" )

                self.load_contexts[ load.loadid ] = load

    def __iter__( self ) -> Iterator[ LoadContexts ] :
        return self.load_contexts.values().__iter__()

    def __contains__( self, item: LoadId ) -> bool :
        return item in self.load_contexts

    def __getitem__( self, item: LoadId ) -> LoadContexts :
        return self.load_contexts[ item ]

    def __len__( self ) -> int :
        return len( self.load_contexts )

    def __setitem__( self, key: LoadId, value: LoadContexts ) -> NoReturn :
        self.load_contexts[ key ] = value

    def add_context( self, context: LoadContexts, allow_overwrite: bool = False ) :
        if not allow_overwrite and context.loadid in self.load_contexts :
            raise Exception( f"Overwritting LoadContext : {context.loadid}" )

        self.load_contexts[ context.loadid ] = context

    def get( self, item: LoadId ) -> Optional[ LoadId ] :
        return self.load_contexts.get( item, None )

    def extract_tag_set( self ) -> Set[ Tag ] :
        return set( self.load_contexts.values() | pipe.map( lambda load : load.tags ) | pipe.traverse() )

    def remove_unoptimizable_loads( self ) -> ContextPath :
        return ContextPath(
            self
            | pipe.where( lambda lc : len( lc.tags ) > 1 )
            | pipe.map( lambda lc : lc.remove_null_count_contexts() )
            | pipe.where( lambda lc : len( lc.exacts ) + len( lc.partial ) + len( lc.unknown ) > 0 )
        )

    def context_fusion( self ) -> ContextPath :
        return ContextPath(
            self | pipe.map( lambda lc : lc.fusion_optimization() )
        )

    def _equiv_loadcontext( self, loadid1: LoadId, loadid2: LoadId ) -> bool :
        load1 = self.load_contexts[ loadid1 ]
        load2 = self.load_contexts[ loadid2 ]

        if frozenset( load1.tags ) != frozenset( load2.tags ) :
            return False

        if len( load1.exacts ) + len( load1.partial ) == 0 or \
                len( load2.exacts ) + len( load2.partial ) == 0 :
            return True

        exact_set1 = set( load1.exacts | pipe.map( lambda context : (context.tag, context.count) ) )
        exact_set2 = set( load2.exacts | pipe.map( lambda context : (context.tag, context.count) ) )

        partial_set1 = set( load1.partial | pipe.map( lambda context : (frozenset( context.tags ), context.count) ) )
        partial_set2 = set( load2.partial | pipe.map( lambda context : (frozenset( context.tags ), context.count) ) )

        if exact_set1 == exact_set2 and partial_set1 == partial_set2 :
            return True

        if len( load1.exacts ) >= 2 or len( load1.partial ) >= 2 or \
                len( load2.exacts ) >= 2 or len( load2.partial ) >= 2 :
            return False

        if len( load1.exacts ) == 1 and len( load1.partial ) == 0 and \
                len( load2.exacts ) == 1 and len( load2.partial ) == 0 :
            return load1.exacts[ 0 ].tag == load2.exacts[ 0 ].tag

        if len( load1.exacts ) == 0 and len( load1.partial ) == 1 and \
                len( load2.exacts ) == 0 and len( load2.partial ) == 1 :
            return load1.partial[ 0 ].tags == load2.partial[ 0 ].tags

        return False

    def compute_load_eq_class( self ) -> EqClassMap[ LoadId ] :
        load_eqclass = EqClassMap.construct_eqclass_map( self.load_contexts, self._equiv_loadcontext )

        return load_eqclass

    def compute_tag_eq_class( self ) -> EqClassMap[ Tag ] :
        tags = self.extract_tag_set()

        tag_context: Dict[ Tag, Set[ LoadId ] ] = { tag : set() for tag in tags }

        for load in self :
            for tag in load.tags :
                tag_context[ tag ].add( load.loadid )

        eqclass_tags = EqClassMap.construct_eqclass_map(
            tags,
            lambda t1, t2 : tag_context[ t1 ] == tag_context[ t2 ]
        )

        return eqclass_tags

    def apply_eqclass_loads( self, eqclass_loads: EqClassMap[ LoadId ] ) -> ContextPath :
        new_cp = ContextPath()

        # Check if there are load contexts not in the eqclass (an error)
        diff_set = set( self.load_contexts ).difference( set( eqclass_loads.__iter__() ) )
        if len( diff_set ) != 0 :
            raise Exception( f"Found loadids {diff_set} not present in eqclass_loads" )

        for loadid in eqclass_loads.classes() :
            if loadid not in self :
                continue

            load_context = LoadContexts( loadid=loadid, tags=self[ loadid ].tags )

            for equiv_loadid in eqclass_loads[ loadid ] :
                load_context.exacts += self[ equiv_loadid ].exacts
                load_context.partial += self[ equiv_loadid ].partial
                load_context.unknown += self[ equiv_loadid ].unknown

            new_cp.add_context( load_context.fusion_optimization() )

        return new_cp

    def apply_eqclass_tags( self, eqclass_tags: EqClassMap[ Tag ] ) -> ContextPath :
        return ContextPath(
            (lc.apply_eqclass_tags( eqclass_tags ) for lc in self)
        )

    def apply_tag_map( self, tag_map: Dict[Tag, Tag] ) -> ContextPath:
        return ContextPath(
            (lc.apply_tag_map( tag_map ) for lc in self)
        )


CONSTRAINT_LIST = List[ Tuple[ ContextPath, int ] ]


@dataclass
class IlpIntervalProblem( object ) :
    path_contexts: ContextPath
    constraints: CONSTRAINT_LIST
    eqclass_tags: EqClassMap[ Tag ]
    eqclass_loads: EqClassMap[ LoadId ]

    def extract_tag_set( self ) -> Set[ Tag ] :
        tags = self.path_contexts.extract_tag_set()

        for path, _ in self.constraints :
            tags.update( path.extract_tag_set() )

        return tags

    def load_traversal( self ) -> Iterator[ LoadContexts ] :
        return chain( self.path_contexts,
                      self.constraints | pipe.map( lambda x : x[ 0 ] ) | pipe.traverse )

    def path_traversal( self ) -> Iterator[ ContextPath ] :
        yield self.path_contexts
        for path, _ in self.constraints :
            yield path

    def remove_unoptimizable_contexts( self ) -> IlpIntervalProblem :
        # Remove the contexts that only contains 1 tag (i.e. are not optimizable)
        # Supposed to have been applied on the constraints
        return IlpIntervalProblem(
            path_contexts=self.path_contexts.remove_unoptimizable_loads(),
            constraints=self.constraints,
            eqclass_tags=self.eqclass_tags,
            eqclass_loads=self.eqclass_loads
        )

    def inner_contexts_fusion( self ) :
        # Fusion the exact/partial/unknown contexts that have the same tag by summing there count
        return IlpIntervalProblem(
            path_contexts=self.path_contexts.context_fusion(),
            constraints=[(cs.context_fusion(), cost) for cs, cost in self.constraints],
            eqclass_tags=self.eqclass_tags,
            eqclass_loads=self.eqclass_loads
        )

    @staticmethod
    def new_empty() -> IlpIntervalProblem :
        return IlpIntervalProblem(
            path_contexts=ContextPath(),
            constraints=[ ],
            eqclass_loads=EqClassMap(),
            eqclass_tags=EqClassMap()
        )

    def tag_equivalent_class( self ) -> IlpIntervalProblem :
        new_problem = IlpIntervalProblem(
            path_contexts=ContextPath( [ ] ),
            constraints=[ ],
            eqclass_tags=self.eqclass_tags,
            eqclass_loads=self.eqclass_loads
        )

        for path in self.path_traversal() :
            new_problem.eqclass_tags &= path.compute_tag_eq_class()

        if '1' in new_problem.eqclass_tags :
            new_problem.eqclass_tags.map[ '1' ].data.witness = '1'

        new_problem.path_contexts = self.path_contexts.apply_eqclass_tags( new_problem.eqclass_tags )

        new_problem.constraints = [
            (path.apply_eqclass_tags( new_problem.eqclass_tags ), m) for path, m in self.constraints
        ]

        return new_problem

    def load_equivalent_class( self ) -> IlpIntervalProblem :
        # Fusion loads that have the same contexts (if there is 1 or less context other than unknown) and the same tags
        # NOT supposed to have been applied on the constraints

        new_problem = IlpIntervalProblem(
            path_contexts=ContextPath( [ ] ),
            constraints=[ ],
            eqclass_tags=self.eqclass_tags,
            eqclass_loads=self.eqclass_loads
        )

        for path in self.path_traversal() :
            new_problem.eqclass_loads &= path.compute_load_eq_class()

        new_problem.path_contexts = self.path_contexts.apply_eqclass_loads( new_problem.eqclass_loads )

        new_problem.constraints = [
            (path.apply_eqclass_loads( new_problem.eqclass_loads ), m) for path, m in self.constraints
        ]

        return new_problem

    def apply_tag_map( self, tag_map: Dict[Tag, Tag] ) -> IlpIntervalProblem:
        new_problem = IlpIntervalProblem(
            path_contexts=self.path_contexts.apply_tag_map( tag_map ),
            constraints=[ (pc.apply_tag_map( tag_map ), cost) for pc, cost in self.constraints ],
            eqclass_tags=EqClassMap(),
            eqclass_loads=EqClassMap()
        )

        return new_problem


@dataclass
class IlpIntervalSolution( object ) :
    problem: IlpIntervalProblem
    tag_representation: Dict[ Tag, int ]
    interval_order: Dict[ LoadId, Dict[ Tag, int ] ]

    def apply_tag_map( self ) -> IlpIntervalSolution:
        tag_map = { tag: str(new_tag) for tag,new_tag in self.tag_representation.items() }
        new_problem = IlpIntervalSolution(
            problem=self.problem.apply_tag_map( tag_map ),
            tag_representation=self.tag_representation,
            interval_order=self.interval_order
        )

        return new_problem
