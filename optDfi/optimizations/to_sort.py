# coding=utf-8

# def gen_ais_file( entry_file: Path, output_file: Path, executable: Path ) :
#     with open( output_file, "a" ) as f :
#         f.write( "try {\n" )
#         f.write( "include '{entry}'\n".format(entry=entry_file) )
#
#     cmd_template = Template( "$objdump -d $exe | grep 'ebreak' | " + \
#                              "awk '{ sub(\":\", \"\", $$1); print \"instruction 0x\" $$1 \"-0x4 condition always taken;\" }' >> $output" )
#     cmd_line = cmd_template.substitute( objdump="riscv32-unknown-elf-objdump", exe=executable, output=output_file )
#
#     sp.run( cmd_line, shell=True, stdout=sp.PIPE, text=True, encoding='utf-8' )
#     # dfi_tag_partitionning_re = re.compile("^(0x[0-9a-f]*) : s[bhw] : t4,(-?[0-9]*)\(t3\)$", re.MULTILINE)
#     # dfi_tag_partitionning_cmd2 = " | awk '{ sub(\":\", \"\", $1); print \"0x\" $1 \" : \" $3 \" : \" $4}'" \
#     #                              " | grep -E 's[bhw]'" \
#     #                              " | grep -E 't4,-?[0-9]*\(t3\)'"
#     # dfi_tag_partitionning_cmd = " ".join([ebreak_inf_cmd1, dfi_tag_partitionning_cmd2])
#     # dfi_tag_partitionning = sp.run( dfi_tag_partitionning_cmd, shell=True, stdout=sp.PIPE, text=True, encoding='utf-8' )
#     #
#     #
#     # dfi_store_tag_address_re = re.compile("^(0x[0-9a-f]*) : .dfi_store_tag_([0-9]+)_([0-9]+)_([0-9]+)_([0-9]+)$", re.MULTILINE)
#     # dfi_store_tag_address_cmd1 = "{objdump} -x {exe}".format(
#     #     objdump="riscv32-unknown-elf-objdump",
#     #     exe=executable
#     # )
#     # dfi_store_tag_address_cmd2 = " | grep \".dfi_store_tag\" | awk '{ print \"0x\" $1 \" : \" $5 }'"
#     # dfi_store_tag_address_cmd = " ".join([dfi_store_tag_address_cmd1, dfi_store_tag_address_cmd2])
#     #
#     # dfi_store_tag_address = sp.run( dfi_store_tag_address_cmd, shell=True, stdout=sp.PIPE, text=True, encoding='utf-8' )
#     #
#     # stores = dfi_tag_partitionning_re.findall( dfi_tag_partitionning.stdout )
#     # labels = dfi_store_tag_address_re.findall( dfi_store_tag_address.stdout )
#     #
#     # partitionning = []
#     # for store in stores:
#     #     add = store[0]
#     #     cst = store[1]
#     #     for label in labels:
#     #         if add == label[0]:
#     #             tag = label[3]
#     #             if int(tag) == 0:
#     #                 break
#     #             partitionning.append( (add,cst,tag) )
#
#     with open( output_file, "a" ) as f :
#         # for part in partitionning:
#         #     annotation1 = "\ninstruction {add} ".format(add=hex(int(part[0], base=16)+4))
#         #     annotation2 = "{\n\tbegin partitioning:\n"
#         #     annotation3 = "\t\trestrict mem(reg(\"t3\")+{cst}, 2) = [ {tag} ];".format(cst=part[1], tag=part[2])
#         #     annotation4 = "\n}\n"
#         #     annotation = "".join( [annotation1, annotation2, annotation3, annotation4] )
#         #     f.write(annotation)
#         f.write( "}" )
from typing import List, Tuple, Dict
import networkx as nx

from optDfi.analysis import ExtendedDFILoad, LoadAllContextInfo
from optDfi.interface import llvm
from optDfi.interface.a3.WCETPath import WCETPath
from optDfi.interface.a3.misc import ContextID, ModuloInfo


def generate_basic_value_flow_annotations( extended_dfi_loads: List[ ExtendedDFILoad ] ) -> List[ str ] :
    annotations = [ ]
    for load in extended_dfi_loads :
        start_add = load.bin_info.start_add
        end_add = load.bin_info.end_add
        user_annotation = "instruction " + hex(
            start_add ) + " {\n\tenter with : user(\"dfi_load_add\") = reg(\"t3\")" + \
                          ("+" if load.bin_info.offset >= 0 else "") + str( load.bin_info.offset ) + ";\n}\n"
        annotations.append( user_annotation )
        tags = list( map( lambda tag_data : tag_data.tag, load.bin_info.check_data ) )

        for i, tag in enumerate( load.bin_info.check_data ) :
            value_flow_annotation = "instruction " + hex(
                tag.address ) + " {\n\tenter with : mem(user(\"dfi_load_add\"), 2) = " + \
                                    str( tags[ i : ] ) + ";\n}\n"
            annotations.append( value_flow_annotation )

        user_end_annotation = "instruction " + hex(
            end_add ) + " {\n\tenter with : destroy(user(\"dfi_load_add\"));\n}\n"
        annotations.append( user_end_annotation )

    return annotations


def compute_interesting_address_flow( wcet_path: WCETPath, load_context_info: List[ LoadAllContextInfo ] ) :
    # Computing interesting address-flow
    address_flow: Dict[ ModuloInfo, List[ Tuple[ LoadAllContextInfo, ContextID ] ] ] = { }
    for wcet_node in nx.dfs_preorder_nodes( wcet_path, wcet_path.start_id ) :
        for load in load_context_info :
            if wcet_node.bb_id == load.load.a3_bbid_start and not load.singleton_value and not load.fully_known :
                if len( load.context_info[ wcet_node.context_id ].value_set ) > 1 :
                    for area_read in load.context_info[ wcet_node.context_id ].area_read :
                        if area_read.min != area_read.max :
                            continue
                        if area_read not in address_flow :
                            address_flow[ area_read ] = [ ]
                        address_flow[ area_read ].append( (load, wcet_node.context_id) )

    # Reducing it
    reduced_address_flow = { key : val for key, val in address_flow.items() if len( val ) > 1 }

    return reduced_address_flow


def swap_tag_order_met( metadata: llvm.LLVMMetadata, order: List[ int ] ) :
    tag_to_met = { }
    for met in metadata.content[ 2 : ] :
        tag_to_met[ met.content[ 0 ] ] = met

    new_content = metadata.content[ :2 ]

    for tag in order :
        if tag not in tag_to_met :
            continue
        new_content.append( tag_to_met[ tag ] )

    metadata.content = new_content
