#! /usr/bin/env python3.8
# coding=utf-8

import logging
import subprocess as sp
import re
from pathlib import Path
import shutil
from typing import Dict, List, Optional

from configurations import config
from configurations.benchmarks import benchmarks, Benchmark
from optDfi.misc.utils import color_str, Color
from pyDFI.exceptions import FileNotFoundException, SameTagException
from optDfi.systemInteraction.DFILib import execute_clang, execute_opt, execute_phasar, execute_llc, execute_gcc

def execute_clang_benchmark( benchmark: Benchmark, output_file: Path ):
    flags = config.CLANG_FLAGS + (config.DEBUG_FLAGS if benchmark.debug else [])
    execute_clang( benchmark.sources, output_file, benchmark.includes,
                   flags=flags )

def execute_comet( entry_file: Path, stdout_file: Optional[Path] = None ) -> int :
    """ Execute comet to compute the execution time (approximated) of the executable """
    # Execute the comet simulator on the executable result_file and returns the
    # simulated runtime

    if not entry_file.exists() :
        raise FileNotFoundException( entry_file )

    # print( "\033[1;33m" + "Executing COMET {entry_file}".format( entry_file=entry_file ) + "\033[0m" )

    command_line = [ config.DEFAULT_COMET_PATH, "-f", str( entry_file ) ]
    p = sp.run( command_line, stdout=sp.PIPE, text=True,
                encoding='utf-8' )

    # with open( stdout_file, "w" ) as f :
    #     f.write( p.stdout )

    stdout = p.stdout.split( "\n" )
    runtime = -1

    if len(stdout) >= 2:
        runtime = int( stdout[-2] )
    else:
        raise Exception( "Failed to recover the runtime by comet : " + str( entry_file ) )

    if runtime == -1 :
        raise Exception( "Failed to recover the runtime by comet : " + str( entry_file ) )

    return runtime


def gen_ais_file( entry_file: Path, output_file: Path, executable: Path, dfi: bool ) :
    if not entry_file.exists() :
        raise FileNotFoundException( entry_file )
    if not executable.exists() :
        raise FileNotFoundException( executable )

    shutil.copyfile( entry_file, output_file )

    if dfi :
        with open( output_file, "a" ) as f :
            f.write( "try {\n" )

        ebreak_inf_cmd1 = "{objdump} -d {exe}".format(
            objdump="riscv32-unknown-elf-objdump",
            exe=executable
        )
        ebreak_inf_cmd2 = "| grep \"ebreak\" | awk '{ sub(\":\", \"\", $1); print \"instruction 0x\" $1 \"-0x4 condition always taken;\" }'"
        ebreak_inf_cmd3 = ">> {output}".format( output=output_file )

        ebreak_inf_cmd = " ".join( [ ebreak_inf_cmd1, ebreak_inf_cmd2, ebreak_inf_cmd3 ] )

        p = sp.run( ebreak_inf_cmd, shell=True, stdout=sp.PIPE, text=True, encoding='utf-8' )

        # dfi_tag_partitionning_re = re.compile("^(0x[0-9a-f]*) : s[bhw] : t4,(-?[0-9]*)\(t3\)$", re.MULTILINE)
        # dfi_tag_partitionning_cmd2 = " | awk '{ sub(\":\", \"\", $1); print \"0x\" $1 \" : \" $3 \" : \" $4}'" \
        #                              " | grep -E 's[bhw]'" \
        #                              " | grep -E 't4,-?[0-9]*\(t3\)'"
        # dfi_tag_partitionning_cmd = " ".join([ebreak_inf_cmd1, dfi_tag_partitionning_cmd2])
        # dfi_tag_partitionning = sp.run( dfi_tag_partitionning_cmd, shell=True, stdout=sp.PIPE, text=True, encoding='utf-8' )
        #
        #
        # dfi_store_tag_address_re = re.compile("^(0x[0-9a-f]*) : .dfi_store_tag_([0-9]+)_([0-9]+)_([0-9]+)_([0-9]+)$", re.MULTILINE)
        # dfi_store_tag_address_cmd1 = "{objdump} -x {exe}".format(
        #     objdump="riscv32-unknown-elf-objdump",
        #     exe=executable
        # )
        # dfi_store_tag_address_cmd2 = " | grep \".dfi_store_tag\" | awk '{ print \"0x\" $1 \" : \" $5 }'"
        # dfi_store_tag_address_cmd = " ".join([dfi_store_tag_address_cmd1, dfi_store_tag_address_cmd2])
        #
        # dfi_store_tag_address = sp.run( dfi_store_tag_address_cmd, shell=True, stdout=sp.PIPE, text=True, encoding='utf-8' )
        #
        # stores = dfi_tag_partitionning_re.findall( dfi_tag_partitionning.stdout )
        # labels = dfi_store_tag_address_re.findall( dfi_store_tag_address.stdout )
        #
        # partitionning = []
        # for store in stores:
        #     add = store[0]
        #     cst = store[1]
        #     for label in labels:
        #         if add == label[0]:
        #             tag = label[3]
        #             if int(tag) == 0:
        #                 break
        #             partitionning.append( (add,cst,tag) )

        with open( output_file, "a" ) as f :
            # for part in partitionning:
            #     annotation1 = "\ninstruction {add} ".format(add=hex(int(part[0], base=16)+4))
            #     annotation2 = "{\n\tbegin partitioning:\n"
            #     annotation3 = "\t\trestrict mem(reg(\"t3\")+{cst}, 2) = [ {tag} ];".format(cst=part[1], tag=part[2])
            #     annotation4 = "\n}\n"
            #     annotation = "".join( [annotation1, annotation2, annotation3, annotation4] )
            #     f.write(annotation)
            f.write( "}" )


def gen_apx_files( executable: Path, ais_file: Path, output_file: Path, xml_file: Path, txt_report: Path ) :
    template: Optional[ str ] = None

    with open( config.APX_TEMPLATE_PATH, "r" ) as f :
        template = "".join( f.readlines() )

    apx = template.format(
        executable=executable,
        xml_report=xml_file,
        ais=ais_file,
        txt_report=txt_report
    )

    with open( output_file, "w" ) as f :
        f.write( apx )


def execute_aiT( entry_file: Path, apx_file: Path, xml_file: Path ) :
    """Execute aiT and compute the WCET of a program """

    wcet_re = re.compile( "<wcet>([0-9]*) cycles</wcet>" )
    command_line = [ "alauncher", "-b", str(apx_file) ]

    logging.info( color_str(f"Executing WCET {entry_file}", Color.ORANGE) )

    cmd = " ".join( command_line )
    p = sp.run( cmd, shell=True )

    if p.returncode != 0 :
        raise Exception( "aiT return status != 0 for " + str( entry_file ) )

    logging.info( color_str(f"Finished execution WCET {entry_file}", Color.ORANGE) )

    with open( xml_file, "r" ) as f :
        for line in f :
            m = wcet_re.match( line )
            if m is not None :
                return int( m.group( 1 ) )

    return -1


# def execute_loopprinter( entry_file: Path, stdout_file: Path, ais_file: Path ) :
#     if not entry_file.exists() :
#         raise FileNotFoundException( entry_file )
#
#     print( "\033[1;33m" + "Executing LOOP PRINTER {entry_file}".format( entry_file=entry_file ) + "\033[0m" )
#
#     port = 4000
#     qemu_script_path = config.DEFAULT_PROJECT_DIR + "/DFI/scripts/auto-qemu.sh"
#     extract_script_path = config.DEFAULT_PROJECT_DIR + "/DFI/tools/pyDFI/extract_ais.py"
#     time = 150
#
#     command_line = "{script} {result_file} {stdout_file} {port} {time}".format(
#         script=qemu_script_path,
#         result_file=entry_file,
#         stdout_file=stdout_file,
#         port=port,
#         time=time
#     )
#
#     sp.run( command_line, shell=True, stdout=sp.PIPE, text=True, encoding='utf-8' )
#
#     command_line_extract = "python3.8 {script} {result_file} > {ais_file}".format(
#         script=extract_script_path,
#         result_file=stdout_file,
#         ais_file=ais_file
#     )
#
#     sp.run( command_line_extract, shell=True, text=True, encoding='utf-8' )


def execute_qemu( entry_file: Path, stdout_file: Path ) -> bool :
    """ Execute QEMU to verify that the executable is indeed correctly finishing """
    # Execute qemu to determine if the executable result_file pass qemu or not
    if not entry_file.exists() :
        raise FileNotFoundException( entry_file )

    print( "\033[1;33m" + f"Executing QEMU {entry_file}" + "\033[0m" )

    port = 4000
    script_path = config.DEFAULT_PROJECT_DIR + "/DFI/scripts/auto-qemu.sh"
    time = 3

    command_line = "{script} {file} {stdout_file} {port} {time}".format(
        script=script_path,
        file=entry_file,
        stdout_file=stdout_file,
        port=port,
        time=time
    )

    p = sp.run( command_line, shell=True, stdout=sp.PIPE, text=True, encoding='utf-8' )

    return p.returncode == 0


def check_benchmarks( benchs: List[ Benchmark ] ) :
    tags = [ ]

    for bench in benchs :
        if bench.tag in tags :
            raise SameTagException( bench.tag )

        tags.append( bench.tag )


class CompileChain( object ) :
    def __init__( self, bench: Benchmark ) :
        self.bench = bench
        self.build_dir = Path( config.DEFAULT_TMP_DIR ) / bench.tag
        self.dfi_dir = self.build_dir / "dfi"
        self.cc_dir = self.build_dir / "cc"
        self.loop_printer_dir = self.build_dir / "loop_printer"
        self.bin_dir = Path( config.DEFAULT_BINARY_DIR ) / bench.tag
        self.executable: Dict[ str, Path ] = dict()

    def generate_dirs( self ) :
        Path( self.build_dir ).mkdir( parents=True, exist_ok=True )
        Path( self.dfi_dir ).mkdir( parents=True, exist_ok=True )
        Path( self.cc_dir ).mkdir( parents=True, exist_ok=True )
        Path( self.loop_printer_dir ).mkdir( parents=True, exist_ok=True )
        Path( self.bin_dir ).mkdir( parents=True, exist_ok=True )

    def build_common( self ) :
        execute_clang_benchmark( benchmark=self.bench,
                       output_file=self.build_dir / (self.bench.tag + ".linked.ll") )

        execute_opt( entry_file=self.build_dir / (self.bench.tag + ".linked.ll"),
                     output_file=self.build_dir / (self.bench.tag + ".aligned.ll"),
                     passes=[ "O1", "dfialignment" ] )

    def build_dfi_prebackend( self ) :
        execute_opt( entry_file=self.build_dir / (self.bench.tag + ".aligned.ll"),
                     output_file=self.dfi_dir / (self.bench.tag + ".annotated.ll"),
                     passes=[ "dfiannotation" ] )

        execute_phasar( entry_file=self.dfi_dir / (self.bench.tag + ".annotated.ll"),
                        output_file=self.dfi_dir / (self.bench.tag + ".analyzed.ll") )

        print( "\033[1;34m" "Starting optimization" "\033[0m" )
        entry_file: Path = self.dfi_dir / (self.bench.tag + ".analyzed.ll")

        execute_opt( entry_file=entry_file,
                     output_file=self.dfi_dir / (self.bench.tag + ".intrinsic.ll"),
                     passes=self.bench.passes + [ "dfiintrinsicinstr" ] )

    def build_loop_printer_prebackend( self ) :
        execute_opt( entry_file=self.build_dir / (self.bench.tag + ".aligned.ll"),
                     output_file=self.loop_printer_dir / (self.bench.tag + ".preprint.ll"),
                     passes=[ "dfiprintloopintro" ] )

        execute_opt( entry_file=self.loop_printer_dir / (self.bench.tag + ".preprint.ll"),
                     output_file=self.loop_printer_dir / (self.bench.tag + ".loopprint.ll"),
                     passes=[ "dfiprintloop" ] )

    def compile_backend( self, linker_script: str, input_file: Path, assembly_file: Path,
                         output_file: Path ) :
        execute_llc( entry_file=input_file,
                     output_file=assembly_file,
                     flags=config.LLC_FLAGS )

        # smooth_asm( entry_file=assembly_file )

        execute_gcc( entry_file=assembly_file,
                     output=output_file,
                     linker_script=linker_script,
                     debug=self.bench.debug,
                     flags=config.GCC_FLAGS )

    def full_compilation( self ) :
        self.generate_dirs()
        self.build_common()
        self.build_dfi_prebackend()
        self.build_loop_printer_prebackend()

        print( "\033[1;34m" "Compiling CC" "\033[0m" )

        # CC comet
        self.compile_backend(
            linker_script=config.LINKER_SCRIPTS[ "comet" ],
            input_file=self.build_dir / (self.bench.tag + ".aligned.ll"),
            assembly_file=self.cc_dir / (self.bench.tag + ".s"),
            output_file=self.bin_dir / (self.bench.tag + ".elf.cc.comet")
        )
        self.executable[ "cc-comet" ] = self.bin_dir / (self.bench.tag + ".elf.cc.comet")

        # CC QEMU/WCET
        self.compile_backend(
            linker_script=config.LINKER_SCRIPTS[ "qemu-wcet" ],
            input_file=self.build_dir / (self.bench.tag + ".aligned.ll"),
            assembly_file=self.cc_dir / (self.bench.tag + ".s"),
            output_file=self.bin_dir / (self.bench.tag + ".elf.cc.qemu")
        )
        self.executable[ "qemu-comet" ] = self.bin_dir / (self.bench.tag + ".elf.cc.qemu")

        print( "\033[1;34m" "Compiling DFI" "\033[0m" )
        # DFI comet
        self.compile_backend(
            linker_script=config.LINKER_SCRIPTS[ "comet" ],
            input_file=self.dfi_dir / (self.bench.tag + ".intrinsic.ll"),
            assembly_file=self.dfi_dir / (self.bench.tag + ".s"),
            output_file=self.bin_dir / (self.bench.tag + ".elf.dfi.comet")
        )
        self.executable[ "dfi-comet" ] = self.bin_dir / (self.bench.tag + ".elf.dfi.comet")

        # DFI QEMU/WCET
        self.compile_backend(
            linker_script=config.LINKER_SCRIPTS[ "qemu-wcet" ],
            input_file=self.dfi_dir / (self.bench.tag + ".intrinsic.ll"),
            assembly_file=self.dfi_dir / (self.bench.tag + ".s"),
            output_file=self.bin_dir / (self.bench.tag + ".elf.dfi.qemu")
        )
        self.executable[ "dfi-qemu" ] = self.bin_dir / (self.bench.tag + ".elf.dfi.qemu")

        print( "\033[1;34m" "Compiling Loop printer" "\033[0m" )

        # LOOP PRINTER
        self.compile_backend(
            linker_script=config.LINKER_SCRIPTS[ "qemu-wcet" ],
            input_file=self.loop_printer_dir / (self.bench.tag + ".loopprint.ll"),
            assembly_file=self.loop_printer_dir / (self.bench.tag + ".s"),
            output_file=self.bin_dir / (self.bench.tag + ".elf.loopprint.qemu")
        )
        self.executable[ "loopprint" ] = self.bin_dir / (self.bench.tag + ".elf.loopprint.qemu")

def generate_compile_chain( benchs: List[ Benchmark ] ) -> List[ CompileChain ]:
    return [ CompileChain( bench ) for bench in benchs ]

def build_bench( benchs: List[ CompileChain ] ) :
    for chain in benchs :
        try :
            print( "\033[1;34m" "===== " + chain.bench.tag.upper() + " =====" "\033[0m" )
            chain.full_compilation()

        except FileNotFoundException as e :
            raise e

def perform_analysis( benchs: List[ Benchmark ] ) :
    """ Execute qemu, comet & aiT to retrieve data about the benchmarks """
    qemu_output_path = Path( config.DEFAULT_OUTPUT_DIR ) / "qemu"
    comet_output_path = Path( config.DEFAULT_OUTPUT_DIR ) / "comet"
    wcet_output_path = Path( config.DEFAULT_OUTPUT_DIR ) / "wcet"
    loop_printer_output_path = Path( config.DEFAULT_OUTPUT_DIR ) / "loop_printer"

    qemu_output_path.mkdir( parents=True )
    comet_output_path.mkdir( parents=True )
    wcet_output_path.mkdir( parents=True )
    loop_printer_output_path.mkdir( parents=True )

    qemu_data_path = Path( config.DEFAULT_DATA_DIR ) / "qemu_results.txt"
    comet_data_path = Path( config.DEFAULT_DATA_DIR ) / "comet_results.txt"
    wcet_data_path = Path( config.DEFAULT_DATA_DIR ) / "wcet_results.txt"

    print( "\033[1;34m" "Preparation phase of analysis" "\033[0m" )

    # Preparation phase
    with open( qemu_data_path, "w" ) as f :
        f.write( "BENCH; CC; DFI\n" )

    with open( comet_data_path, "w" ) as f :
        f.write( "BENCH; CC; DFI; FACTOR\n" )

    with open( wcet_data_path, "w" ) as f :
        f.write( "BENCH; CC; DFI; FACTOR\n" )

    for bench in benchs :
        # Check that the executable exists
        base = config.DEFAULT_BINARY_DIR + bench.tag + "/" + bench.tag + ".elf"
        if not Path( base + ".dfi.qemu" ).exists() :
            raise FileNotFoundException( Path( base + ".dfi.qemu" ) )
        if not Path( base + ".cc.qemu" ).exists() :
            raise FileNotFoundException( Path( base + ".cc.qemu" ) )
        if not Path( base + ".dfi.comet" ).exists() :
            raise FileNotFoundException( Path( base + ".dfi.comet" ) )
        if not Path( base + ".cc.comet" ).exists() :
            raise FileNotFoundException( Path( base + ".cc.comet" ) )
        if bench.loop_printer and not Path( base + ".loopprint.qemu" ).exists() :
            raise FileNotFoundException( Path( base + ".loopprint.qemu" ) )

        if bench.qemu :
            (qemu_output_path / bench.tag).mkdir( parents=True )
        if bench.wcet is not None :
            # Generate the apx result_file for aiT
            ais_base_file = Path( bench.wcet )
            bench_output_dir = wcet_output_path / bench.tag
            Path( bench_output_dir ).mkdir( parents=True )
            gen_ais_file( entry_file=ais_base_file, output_file=bench_output_dir / (bench.tag + ".cc.ais"),
                          executable=Path(base + ".cc.qemu"), dfi=False )
            gen_ais_file( entry_file=ais_base_file, output_file=bench_output_dir / (bench.tag + ".dfi.ais"),
                          executable=Path(base + ".dfi.qemu"), dfi=True )
            gen_apx_files( executable=Path(base + ".cc.qemu"),
                           ais_file=bench_output_dir / (bench.tag + ".cc.ais"),
                           output_file=bench_output_dir / (bench.tag + ".cc.apx"),
                           xml_file=bench_output_dir / (bench.tag + ".cc.xml"),
                           txt_report=bench_output_dir / (bench.tag + ".cc.txt") )
            gen_apx_files( executable=Path(base + ".dfi.qemu"),
                           ais_file=bench_output_dir / (bench.tag + ".dfi.ais"),
                           output_file=bench_output_dir / (bench.tag + ".dfi.apx"),
                           xml_file=bench_output_dir / (bench.tag + ".dfi.xml"),
                           txt_report=bench_output_dir / (bench.tag + ".dfi.txt") )
        if bench.comet :
            (comet_output_path / bench.tag).mkdir( parents=True )
        if bench.loop_printer :
            (loop_printer_output_path / bench.tag).mkdir( parents=True )

    # Analysis phase
    for bench in benchs :
        binary_path = config.DEFAULT_BINARY_DIR + bench.tag + "/"
        exe_base = binary_path + bench.tag + ".elf"

        # Execute qemu
        qemu_dfi = True
        qemu_cc = True
        if bench.qemu :
            # DFI
            qemu_dfi = execute_qemu( entry_file=Path(exe_base + ".dfi.qemu"),
                                     stdout_file=qemu_output_path / bench.tag / (bench.tag + ".dfi.txt") )

            # CC
            qemu_cc = execute_qemu( entry_file=Path(exe_base + ".cc.qemu"),
                                    stdout_file=qemu_output_path / bench.tag / (bench.tag + ".cc.txt") )

            with open( qemu_data_path, "a" ) as f :
                f.write( "{bench}; {cc}; {dfi}\n".format(
                    bench=bench.tag,
                    cc="OK" if qemu_cc else "FAILED",
                    dfi="OK" if qemu_dfi else "FAILED",
                ) )

        if bench.comet :
            if not qemu_cc or not qemu_dfi :
                raise Exception( "Failed QEMU check before executing COMET" )
            # DFI
            comet_dfi = execute_comet( entry_file=Path( exe_base + ".dfi.comet" ),
                                       stdout_file=comet_output_path / bench.tag / (bench.tag + ".dfi.txt") )

            # CC
            comet_cc = execute_comet( entry_file=Path( exe_base + ".cc.comet" ),
                                      stdout_file=comet_output_path / bench.tag / (bench.tag + ".cc.txt") )

            with open( comet_data_path, "a" ) as f :
                f.write( "{bench}; {cc}; {dfi}; {factor}\n".format(
                    bench=bench.tag,
                    cc=comet_cc,
                    dfi=comet_dfi,
                    factor=comet_dfi / comet_cc
                ) )

        if bench.wcet is not None :
            if not qemu_cc or not qemu_dfi :
                raise Exception( "Failed QEMU check before executing COMET" )

            # CC
            wcet_cc = execute_aiT( entry_file=Path( exe_base + ".cc.qemu" ),
                                   apx_file=wcet_output_path / bench.tag / (bench.tag + ".cc.apx"),
                                   xml_file=wcet_output_path / bench.tag / (bench.tag + ".cc.xml") )

            wcet_dfi = execute_aiT( entry_file=Path( exe_base + ".dfi.qemu" ),
                                    apx_file=wcet_output_path / bench.tag / (bench.tag + ".dfi.apx"),
                                    xml_file=wcet_output_path / bench.tag / (bench.tag + ".dfi.xml") )

            with open( wcet_data_path, "a" ) as f :
                f.write( "{bench}; {cc}; {dfi}; {factor}\n".format(
                    bench=bench.tag,
                    cc=wcet_cc,
                    dfi=wcet_dfi,
                    factor=wcet_dfi / wcet_cc
                ) )

def main() :
    # Perform mkdir on the config directories
    if not config.ONLY_ANALYSIS :
        sp.run( "rm -rf " + config.DEFAULT_BUILD_DIR, shell=True )
        Path( config.DEFAULT_BINARY_DIR ).mkdir( parents=True, exist_ok=True )
    else :
        sp.run( "rm -rf " + config.DEFAULT_OUTPUT_DIR + " " + config.DEFAULT_DATA_DIR, shell=True )

    Path( config.DEFAULT_TMP_DIR ).mkdir( parents=True, exist_ok=True )
    Path( config.DEFAULT_OUTPUT_DIR ).mkdir( parents=True, exist_ok=True )
    Path( config.DEFAULT_DATA_DIR ).mkdir( parents=True, exist_ok=True )

    check_benchmarks( benchmarks )
    chains = generate_compile_chain( benchmarks )

    # Build the bench for examination
    if not config.ONLY_ANALYSIS :
        build_bench( chains )

    # Perform the analysis on the benchs
    perform_analysis( benchmarks )


if __name__ == "__main__" :
    main()
