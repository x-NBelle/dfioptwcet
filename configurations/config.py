# coding=utf-8
from pathlib import Path

ARCH_FLAGS = [
    "-march=riscv32",
    "-mattr=+m"
]
DEBUG_FLAGS = [
    "-g",
]
OPT_FLAGS = [ ]
CLANG_FLAGS = [
    "-fno-builtin",
    "-ffreestanding",
    "-fno-discard-value-names",
    "--target=riscv32",
    "-O1"  # O1
]
LLC_FLAGS = [
    "-O1",
    # "--debug-pass=Executions",
    # "--dfi-lschain-debug-cutted",
    # "--dfi-lschain-debug-print-all-lschains",
    # "--dfi-lschain-debug-liveness-lschain",
    # "--dfi-debug-mf=matrix1_main",
    # "--dfi-lschain-debug-skipped-offset"
]
GCC_FLAGS = [
    "-mcmodel=medany",
    "-O0",
    "-nostartfiles",
    "-nolibc",
    "-Wno-trigraphs",  # Disable huge warnings on trigraphs that bloat the output in benchmarks
    # "-Wl,--orphan-handling=warn"
]

LINKER_SCRIPTS = {
    "comet" : "linker-comet.ld",
    "qemu-wcet" : "linker-qemu-wcet.ld"
}

DEFAULT_PROJECT_DIR = Path( "/home/nbellec/research/thesis" )

DEFAULT_LLVM_PATH = DEFAULT_PROJECT_DIR / "llvm-project"
DEFAULT_PHASAR_PATH = DEFAULT_PROJECT_DIR / "phasar/build/tools/dfi_binding/dfi_binding"
DEFAULT_GCC_RISCV32_PATH = Path( "/opt/riscv32i/bin/riscv32-unknown-elf-gcc" )
DEFAULT_RISCV32_OBJDUMP_PATH = Path( "/opt/riscv32i/bin/riscv32-unknown-elf-objdump" )
DEFAULT_LINK_PATH = DEFAULT_PROJECT_DIR / "DFI/tests/sources/bootloader/"
DEFAULT_COMET_PATH = DEFAULT_PROJECT_DIR / "Comet/build/bin/comet.sim"
DEFAULT_QEMU_PATH = DEFAULT_PROJECT_DIR / "qemu/build/qemu-system-riscv32"
DEFAULT_BUILD_DIR = DEFAULT_PROJECT_DIR / "DFI/tests/build/"
DEFAULT_BINARY_DIR = DEFAULT_BUILD_DIR / "bin/"
DEFAULT_TMP_DIR = DEFAULT_BUILD_DIR / "llvm/"
DEFAULT_OUTPUT_DIR = DEFAULT_BUILD_DIR / "outputs/"
DEFAULT_DATA_DIR = DEFAULT_BUILD_DIR / "data/"

APX_TEMPLATE_PATH = DEFAULT_PROJECT_DIR / "DFI/tests/templates/template.apx"

ONLY_ANALYSIS = False
