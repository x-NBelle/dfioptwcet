# coding=utf-8
from typing import List, Optional

from configurations import config
from optDfi.experiment.Benchmark import Benchmark, findCFiles
from pathlib import Path

source_dir = config.DEFAULT_PROJECT_DIR / "DFI/tests/sources/"
debug = False

classic_optimization_passes = [
    "dfioptequivalentclasses",
    "dfioptimizations",
    "dfiopttagcheck"
]

def generateBenchmark( bench_dir: Path, passes: List[ str ], prefix: str,
                       other_sources: Optional[ List[ Path ] ] = None,
                       other_includes: Optional[ List[ Path ] ] = None,
                       ais_file: Optional[ Path ] = None,
                       entry_point: Optional[ str ] = None ) -> Benchmark :
    global debug

    entry_function = entry_point if entry_point is not None else "main"
    if other_sources is None :
        other_sources = [ ]

    tag = prefix + bench_dir.stem

    return Benchmark(
        sources=findCFiles( bench_dir ) + other_sources,
        includes=[ bench_dir ] + (other_includes if other_includes is not None else [ ]),
        tag=tag,
        clang_flags=config.CLANG_FLAGS + (config.DEBUG_FLAGS if debug else [ ]),
        opt_flags=config.OPT_FLAGS,
        opt_passes=passes,
        llc_flags=config.LLC_FLAGS,
        gcc_flags=config.GCC_FLAGS,
        ais_file=ais_file,
        entry_point=entry_function
    )


# --- BENCHMARK DIRECTORIES ---
false_positive_dir = source_dir / "others/"

base_taclebench_dir = source_dir / "taclebench/"

app_taclebench_dir = base_taclebench_dir / "app/"
kernel_taclebench_dir = base_taclebench_dir / "kernel/"
sequential_taclebench_dir = base_taclebench_dir / "sequential/"
test_taclebench_dir = base_taclebench_dir / "test/"
parallel_taclebench_dir = base_taclebench_dir / "parallel/"

# --- BENCHMARK NAME ---
false_positiv_benchname = [
    # "overflow",
    # "not_overflow",
    # "alignement_test"
]

kernel_taclebench_benchname = [
    "binarysearch",
    # "bitcount",  # recursion
    # "bitonic", # Recursion
    "bsort",
    "complex_updates",
    "cosf",
    "countnegative",
    "cubic",
    "deg2rad",
    # "fac",  # recursion
    "fft",          # QEMU witness fails (return value)
    "filterbank",
    "fir2dim",
    "iir",          # LSChain kernel pseudo-inst skipped -- Normal (due to load after load optimization)
    "insertsort",
    "isqrt",
    "jfdctint",
    "lms",
    "ludcmp",
    "matrix1",
    "md5",
    "minver",
    "pm",
    "prime",
    # "quicksort",  # recursion
    "rad2deg",
    # "recursion",  # recursion
    # "sha",          # QEMU SOTA fails, read in sha_wordcopy_fwd_aligned 8 bytes too much
    "st",
]

sequential_taclebench_benchname = [
    "adpcm_dec",
    "adpcm_enc",        # QEMU witness fails (ret val)
    # "ammunition",     # recursion
    # "anagram",        # recursion
    "audiobeam",
    "cjpeg_transupp",   # QEMU witness fails (ret val)
    "cjpeg_wrbmp",
    "dijkstra",
    "epic",
    "fmref",
    "g723_enc",
    "gsm_dec",
    "gsm_enc",
    "h264_dec",           # QEMU witness fails (ret val)
    "huff_dec",
    # "huff_enc",         # recursion
    "mpeg2",
    "ndes",
    "petrinet",
    # "rijndael_dec",     # QEMU/aiT SOTA fails / non-initialized value read
    # "rijndael_enc",     # QEMU/aiT SOTA fails / non-initialized value read
    "statemate",          # Should fail (due to comparison with constant out of range in return) but does not
    "susan",              # QEMU fails in opt: -O1 because the compiler generates an access before write to the l variable in susan_thin
                          # This problem do not arise in O0
]

test_taclebench_benchname = [
    "cover",
    "duff",
    "test3",
]

app_taclebench_benchname = [
    "lift",
    "powerwindow"
]

# --- BENCHMARK GENERATION ---
false_positiv_benchmarks = [
    generateBenchmark( false_positive_dir / bench,
                       other_includes=[ source_dir / "bootloader/" ],
                       passes=classic_optimization_passes,
                       prefix="false_positiv_",
                       ais_file=None
                       ) for bench in false_positiv_benchname
]

lift_taclebench_benchmarks = [
    generateBenchmark( app_taclebench_dir / "lift",
                       passes=classic_optimization_passes,
                       prefix="",
                       ais_file=app_taclebench_dir / "lift/lift_O1.ais",
                       entry_point="lift_main"
                       )
] if "lift" in app_taclebench_benchname else [ ]

powerwindow_taclebench_benchmarks = [
    generateBenchmark( app_taclebench_dir / "powerwindow",
                       other_includes=[ app_taclebench_dir / "powerwindow/powerwindow_HeaderFiles" ],
                       passes=classic_optimization_passes,
                       prefix="",
                       ais_file=app_taclebench_dir / "powerwindow/powerwindow_O1.ais",
                       entry_point="powerwindow_main"
                       )
] if "powerwindow" in app_taclebench_benchname else [ ]

app_taclebench_benchmarks = lift_taclebench_benchmarks + powerwindow_taclebench_benchmarks

sequential_taclebench_benchmarks = [
    generateBenchmark( sequential_taclebench_dir / bench,
                       passes=classic_optimization_passes,
                       prefix="",
                       ais_file=sequential_taclebench_dir / bench / f"{bench}_O1.ais",
                       entry_point=f"{bench}_main" if bench != "gsm_dec" else "main"
                       ) for bench in sequential_taclebench_benchname
]

test_taclebench_benchmarks = [
    generateBenchmark( test_taclebench_dir / bench,
                       passes=classic_optimization_passes,
                       prefix="",
                       ais_file=test_taclebench_dir / bench / f"{bench}_O1.ais",
                       entry_point=f"{bench}_main"
                       ) for bench in test_taclebench_benchname
]

kernel_taclebench_benchmarks = [
    generateBenchmark( kernel_taclebench_dir / bench,
                       passes=classic_optimization_passes,
                       prefix="",
                       ais_file=kernel_taclebench_dir / bench / f"{bench}_O1.ais",
                       entry_point=f"{bench}_main"
                       ) for bench in kernel_taclebench_benchname
]

debie_benchmark = [
    # generateBenchmark( parallel_taclebench_dir / "DEBIE/code",
    #                    other_sources = [
    #                         parallel_taclebench_dir / "DEBIE/code/harness/harness.c",
    #                         parallel_taclebench_dir / "DEBIE/code/arch/riscv/target.c",
    #                         parallel_taclebench_dir / "DEBIE/code/arch/riscv/wcc_memcpy.c"
    #                     ],
    #                    prefix="DEBIE_",
    #                    passes=classic_optimization_passes,
    #                    other_includes = [
    #                         parallel_taclebench_dir / "DEBIE/code/harness/",
    #                         parallel_taclebench_dir / "DEBIE/code/arch/riscv"
    #                    ],
    #                    ais_file=parallel_taclebench_dir / "DEBIE/DEBIE_O1.ais"
    # )
]

papabench_benchmark = [
    # generateBenchmark( parallel_taclebench_dir / "PapaBench/sw/airborne/fly_by_wire",
    #                    other_sources = findCFiles(parallel_taclebench_dir / "PapaBench/sw/lib/c/"),
    #                    prefix="PapaBench_",
    #                    passes=classic_optimization_passes,
    #                    other_includes = [
    #                         parallel_taclebench_dir / "PapaBench/sw/includes",
    #                         parallel_taclebench_dir / "PapaBench/sw/includes/c",
    #                         parallel_taclebench_dir / "PapaBench/arch/include/avr/arch/"
    #                    ],
    #                    ais_file=parallel_taclebench_dir / "PapaBench/PapaBench_O1.ais"
    # )
]

benchmarks = false_positiv_benchmarks + \
             kernel_taclebench_benchmarks + \
             sequential_taclebench_benchmarks + \
             test_taclebench_benchmarks + \
             app_taclebench_benchmarks + \
             debie_benchmark + \
             papabench_benchmark
