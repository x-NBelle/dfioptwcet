# coding=utf-8

from typing import List
from optDfi.interface.objdump import ObjdumpInstruction

def construct_lui_inst_list( size: int, add_start: int ) -> List[ObjdumpInstruction]:
    return [
        ObjdumpInstruction(
            address=add_start + 4*i,
            name="lui",
            operands="a2,0x80008",
            comment=""
        ) for i in range(size)
    ]