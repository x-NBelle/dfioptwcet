# coding=utf-8

import pytest
import networkx as nx
from operator import eq
import optDfi.interface.a3 as a3
import tests.data.data_xml as data_xml


@pytest.fixture( scope="session" )
def xpath_tag_to_routines() :
    return [ "./", "wcet_analysis_task", "cfg_value_analysis", "routines", "routine" ]


@pytest.fixture( scope="session" )
def xpath_tag_to_block( xpath_tag_to_routine ) :
    return xpath_tag_to_routine + [ "block" ]


@pytest.fixture( scope="function" )
def a3_ex() :
    return a3.InterfaceAiTAlternatif( "tests/data/xml/loop.xml" )


class TestAiTInterfaceSlow :
    """
    Tests of the a3 interface that may be slow
    """

    @pytest.mark.slow
    @pytest.mark.parametrize(
        "test_case,expected_icfg",
        [
            ("simple", data_xml.simple_icfg),
            ("branch", data_xml.branch_icfg),
            ("loop", data_xml.loop_icfg),
            ("multi_loops", data_xml.multi_loops_icfg),
            ("call", data_xml.call_icfg),
            ("call_in_loop", data_xml.call_in_loop_icfg),
            ("multi_calls", data_xml.multi_calls_icfg),
        ] )
    def test_icfg( self, test_case, expected_icfg ) :
        inter = a3.InterfaceAiTAlternatif( "tests/data/xml/{r}.xml".format( r=test_case ) )

        assert test_case and inter.test_hypothesis()

        inter.recover_icfg()

        def node_match( at1, at2 ) :
            for key in at1.keys() :
                if type( at1[ key ] ) == a3.CFG :
                    continue
                if not eq( at1[ key ], at2[ key ] ) :
                    return False
            return True

        assert test_case and nx.is_isomorphic(
            inter.icfg,
            expected_icfg,
            node_match=node_match
        )
        for func in expected_icfg.nodes() :
            expected_cfg = expected_icfg.nodes[ func ][ "cfg" ]
            computed_cfg = inter.icfg.nodes[ func ][ "cfg" ]

            assert func and func in inter.icfg.nodes()
            assert func and nx.is_isomorphic(
                computed_cfg,
                expected_cfg
            )

            for bb in expected_cfg :
                assert bb and bb in computed_cfg

    @pytest.mark.slow
    @pytest.mark.parametrize(
        "test_case,expected_wcet_path",
        [
            ("simple", data_xml.simple_wcet_path),
            ("branch", data_xml.branch_wcet_path),
            ("loop", data_xml.loop_wcet_path),
            ("multi_loops", data_xml.multi_loops_wcet_path),
            ("call", data_xml.call_wcet_path),
            ("call_in_loop", data_xml.call_in_loop_wcet_path),
            ("multi_calls", data_xml.multi_calls_wcet_path),
        ] )
    def test_wcet_path( self, test_case, expected_wcet_path ) :
        inter = a3.InterfaceAiTAlternatif( "tests/data/xml/{r}.xml".format( r=test_case ) )

        inter.recover_wcet_path()

        assert test_case and nx.is_isomorphic(
            inter.wcet_path,
            expected_wcet_path,
            node_match=eq
        )

        for node in expected_wcet_path.nodes() :
            assert node and node in inter.wcet_path.nodes()


class TestAiTInterface :
    """
    Tests for the a3
    """

    def test_gen_xpath( self, xpath_tag_to_routines ) :
        assert a3.InterfaceAiTAlternatif._gen_xpath( [ "./" ] ) == "./"
        assert a3.InterfaceAiTAlternatif._gen_xpath( [ "./", "routines" ] ) == ".//a3:routines"
        assert a3.InterfaceAiTAlternatif._gen_xpath( xpath_tag_to_routines ) == \
               ".//a3:wcet_analysis_task/a3:cfg_value_analysis/a3:routines/a3:routine"
        assert a3.InterfaceAiTAlternatif._gen_xpath(
            [ "./", "routines", "..[@type='normal']" ] ) == ".//a3:routines/..[@type='normal']"



# class TestMutationKillerAiTInterface( object ) :
#     """
#     Class of test specialized in killing boring mutants
#     """
#
#     @pytest.mark.parametrize(
#         "namedtuple_class,name",
#         [
#             (a3.WCET_Node, "WCET_Node"),
#         ]
#     )
#     def test_namedtuple( lc1, namedtuple_class, name ) :
#         assert namedtuple_class.__name__ == name
