# # coding=utf-8
from typing import Tuple

from optDfi.optimizations.ilp_opt.ilp_problem.equivalence_map import EqClassMap
from optDfi.optimizations.ilp_opt.ilp_problem.problem import IlpIntervalProblem, \
    ContextPath
from optDfi.optimizations.ilp_opt.ilp_problem.context import ExactContext, PartialContext, UnknownContext, \
    LoadContexts

from tests.ilp_problem_test.ilp_context_test import eq_load_context, assert_eq_load_context

EC = ExactContext
PC = PartialContext
UC = UnknownContext
CP = ContextPath
LC = LoadContexts
IIP = IlpIntervalProblem


def eq_context_path( cp1: ContextPath, cp2: ContextPath ) -> bool :
    if set( cp1.load_contexts.keys() ) != set( cp2.load_contexts.keys() ) :
        return False

    for lc in cp1 :
        if not eq_load_context( cp1[ lc.loadid ], cp2[ lc.loadid ] ) :
            return False

    return True


def assert_eq_context_path( cp1: ContextPath, cp2: ContextPath ) -> None :
    assert set( cp1.load_contexts.keys() ) == set( cp2.load_contexts.keys() )

    for lc in cp1 :
        assert_eq_load_context( cp1[ lc.loadid ], cp2[ lc.loadid ] )


def assert_eq_eqclass( ec1: EqClassMap, ec2: EqClassMap ) -> None :
    assert set( ec1 ) == set( ec2 )
    assert len( set( ec1.classes() ) ) == len( set( ec2.classes() ) )

    for c1 in ec1.classes() :
        for c1_eq in ec1[ c1 ] :
            assert ec2.check_equivalence( c1, c1_eq )


def assert_eq_iip( iip1: IIP, iip2: IIP ) -> None :
    assert_eq_context_path( iip1.path_contexts, iip2.path_contexts )
    assert_eq_eqclass( iip1.eqclass_loads, iip2.eqclass_loads )
    assert_eq_eqclass( iip1.eqclass_tags, iip2.eqclass_tags )
    assert len( iip1.constraints ) == len( iip2.constraints )

    for i in range( len( iip1.constraints ) ) :
        c1, m1 = iip1.constraints[ i ]
        c2, m2 = iip2.constraints[ i ]
        assert m1 == m2
        assert_eq_context_path( c1, c2 )


def test_context_path_extract_tag_set() :
    cp1 = CP( (LC( loadid="lid1", tags={ 'A', 'N', 'B' } ),
               LC( loadid="lid2", tags={ 'A', 'D', 'H' } )) )

    assert cp1.extract_tag_set() == { 'A', 'N', 'B', 'D', 'H' }

    cp1 = CP( [
        LC( loadid='lc1',
            tags={ 'A', 'B', 'C' } ),
        LC( loadid='lc2',
            tags={ 'A', 'C', 'D' } ),
        LC( loadid='lc3',
            tags={ 'A', 'E', 'C' } )
    ] )

    assert cp1.extract_tag_set() == { 'A', 'C', 'B', 'D', 'E' }


def test_context_path_remove_unoptimizable_loads() :
    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
                EC( 'A', 5 ),
                EC( 'A', 10 ),
            ],
            tags={ 'A' } ),
        LC( loadid='lid2',
            exacts=[
                EC( 'B', 5 ),
                EC( 'A', 10 ),
            ],
            tags={ 'A', 'B' } ),
    ] )

    cp_test = cp1.remove_unoptimizable_loads()
    cp_ref = CP( [
        LC( loadid='lid2',
            exacts=[
                EC( 'B', 5 ),
                EC( 'A', 10 ),
            ],
            tags={ 'A', 'B' } )
    ] )

    assert_eq_context_path( cp_test, cp_ref )

    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
                EC( 'A', 5 ),
                EC( 'A', 10 ),
            ],
            tags={ 'A' } ),
        LC( loadid='lid2',
            exacts=[
                EC( 'B', 5 ),
                EC( 'A', 0 ),
            ],
            tags={ 'A', 'B' } ),
    ] )

    cp_test = cp1.remove_unoptimizable_loads()
    cp_ref = CP( [
        LC( loadid='lid2',
            exacts=[
                EC( 'B', 5 ),
            ],
            tags={ 'A', 'B' } )
    ] )

    assert_eq_context_path( cp_test, cp_ref )

    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
                EC( 'A', 5 ),
                EC( 'A', 10 ),
            ],
            tags={ 'A' } ),
        LC( loadid='lid2',
            exacts=[
                EC( 'B', 0 ),
                EC( 'A', 0 ),
            ],
            tags={ 'A', 'B' } ),
    ] )

    cp_test = cp1.remove_unoptimizable_loads()
    cp_ref = CP( [ ] )

    assert_eq_context_path( cp_test, cp_ref )


def test_context_path_context_fusion() :
    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
                EC( 'A', 5 ),
                EC( 'A', 10 ),
            ],
            tags={ 'A' } ),
        LC( loadid='lid2',
            exacts=[
                EC( 'B', 5 ),
                EC( 'A', 10 ),
            ],
            tags={ 'A', 'B' } ),
    ] )

    cp_test = cp1.context_fusion()
    cp_ref = CP( [
        LC( loadid='lid1',
            exacts=[
                EC( 'A', 15 ),
            ],
            tags={ 'A' } ),

        LC( loadid='lid2',
            exacts=[
                EC( 'B', 5 ),
                EC( 'A', 10 ),
            ],
            tags={ 'A', 'B' } )
    ] )

    assert_eq_context_path( cp_test, cp_ref )


def test_context_path_equiv_loadcontext() :
    # Check tags equality
    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
                EC( 'A', 5 ),
                EC( 'A', 10 ),
            ],
            tags={ 'A' } ),
        LC( loadid='lid2',
            exacts=[
                EC( 'B', 5 ),
                EC( 'A', 10 ),
            ],
            tags={ 'A', 'B' } ),
    ] )

    assert not cp1._equiv_loadcontext( 'lid1', 'lid2' )

    # Check same exact contexts
    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
                EC( 'A', 100 ),
                EC( 'B', 100 ),
            ],
            unknown=[
                UC( 5 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B' } ),
        LC( loadid='lid2',
            exacts=[
                EC( 'A', 100 ),
                EC( 'B', 100 ),
            ],
            unknown=[
                UC( 25 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B' } ),
    ] )

    assert cp1._equiv_loadcontext( 'lid1', 'lid2' )

    # Check same partial contexts
    cp1 = CP( [
        LC( loadid='lid1',
            partial=[
                PC( frozenset( ('A', 'B') ), 100 ),
                PC( frozenset( ('A', 'C') ), 100 ),
            ],
            unknown=[
                UC( 5 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
        LC( loadid='lid2',
            partial=[
                PC( frozenset( ('A', 'B') ), 100 ),
                PC( frozenset( ('A', 'C') ), 100 ),
            ],
            unknown=[
                UC( 25 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
    ] )

    assert cp1._equiv_loadcontext( 'lid1', 'lid2' )

    # Check same exact + partial contexts
    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
                EC( 'A', 100 ),
                EC( 'B', 25 ),
            ],
            partial=[
                PC( frozenset( ('A', 'B') ), 80 ),
                PC( frozenset( ('A', 'C') ), 100 ),
            ],
            unknown=[
                UC( 5 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
        LC( loadid='lid2',
            exacts=[
                EC( 'A', 100 ),
                EC( 'B', 25 ),
            ],
            partial=[
                PC( frozenset( ('A', 'B') ), 80 ),
                PC( frozenset( ('A', 'C') ), 100 ),
            ],
            unknown=[
                UC( 25 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
    ] )

    assert cp1._equiv_loadcontext( 'lid1', 'lid2' )

    # Check only unknown vs other
    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
            ],
            partial=[
            ],
            unknown=[
                UC( 5 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
        LC( loadid='lid2',
            exacts=[
                EC( 'A', 100 ),
            ],
            partial=[
                PC( frozenset( ('A', 'B') ), 80 ),
                PC( frozenset( ('A', 'C') ), 100 ),
            ],
            unknown=[
                UC( 25 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
    ] )

    assert cp1._equiv_loadcontext( 'lid1', 'lid2' )

    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
                EC( 'A', 100 ),
            ],
            partial=[
                PC( frozenset( ('A', 'B') ), 80 ),
                PC( frozenset( ('A', 'C') ), 100 ),
            ],
            unknown=[
                UC( 5 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
        LC( loadid='lid2',
            exacts=[
            ],
            partial=[
            ],
            unknown=[
                UC( 25 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
    ] )

    assert cp1._equiv_loadcontext( 'lid1', 'lid2' )

    # Check |exacts| / |partial| >= 2
    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
                EC( 'A', 100 ),
            ],
            partial=[
                PC( frozenset( ('A', 'B') ), 80 ),
                PC( frozenset( ('A', 'C') ), 100 ),
            ],
            unknown=[
                UC( 5 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
        LC( loadid='lid2',
            exacts=[
                EC( 'A', 200 )
            ],
            partial=[
            ],
            unknown=[
                UC( 25 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
    ] )

    assert not cp1._equiv_loadcontext( 'lid1', 'lid2' )

    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
                EC( 'A', 100 ),
            ],
            partial=[
            ],
            unknown=[
                UC( 5 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
        LC( loadid='lid2',
            exacts=[
                EC( 'A', 200 )
            ],
            partial=[
                PC( frozenset( ('A', 'B') ), 80 ),
                PC( frozenset( ('A', 'C') ), 100 ),
            ],
            unknown=[
                UC( 25 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
    ] )

    assert not cp1._equiv_loadcontext( 'lid1', 'lid2' )

    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
                EC( 'A', 100 ),
            ],
            partial=[
            ],
            unknown=[
                UC( 5 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
        LC( loadid='lid2',
            exacts=[
                EC( 'A', 200 )
            ],
            partial=[
                PC( frozenset( ('A', 'B') ), 80 ),
                PC( frozenset( ('A', 'C') ), 100 ),
            ],
            unknown=[
                UC( 25 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
    ] )

    assert not cp1._equiv_loadcontext( 'lid1', 'lid2' )

    # Check same exact tag, no partial
    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
                EC( 'A', 100 ),
            ],
            partial=[
            ],
            unknown=[
                UC( 5 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
        LC( loadid='lid2',
            exacts=[
                EC( 'A', 200 )
            ],
            partial=[
            ],
            unknown=[
                UC( 25 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
    ] )

    assert cp1._equiv_loadcontext( 'lid1', 'lid2' )

    # Check same partial tags, no exact
    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
            ],
            partial=[
                PC( frozenset( ('A', 'B') ), count=20 )
            ],
            unknown=[
                UC( 5 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
        LC( loadid='lid2',
            exacts=[
            ],
            partial=[
                PC( frozenset( ('A', 'B') ), count=40 )
            ],
            unknown=[
                UC( 25 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
    ] )

    assert cp1._equiv_loadcontext( 'lid1', 'lid2' )

    # Check |exacts| |partial| >= 1
    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
            ],
            partial=[
                PC( frozenset( ('A', 'B') ), count=20 )
            ],
            unknown=[
                UC( 5 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
        LC( loadid='lid2',
            exacts=[
            ],
            partial=[
                PC( frozenset( ('A', 'C') ), count=40 )
            ],
            unknown=[
                UC( 25 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
    ] )

    assert not cp1._equiv_loadcontext( 'lid1', 'lid2' )

    # Check |exacts| |partial| >= 1
    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
                EC( 'C', 10 )
            ],
            partial=[
                PC( frozenset( ('A', 'B') ), count=20 )
            ],
            unknown=[
                UC( 5 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
        LC( loadid='lid2',
            exacts=[
            ],
            partial=[
                PC( frozenset( ('A', 'B') ), count=40 )
            ],
            unknown=[
                UC( 25 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
    ] )

    assert not cp1._equiv_loadcontext( 'lid1', 'lid2' )


def test_context_path_apply_load_eq_class() :
    cp1 = CP( [
        LC( loadid='lid1',
            exacts=[
                EC( 'A', 100 ),
            ],
            partial=[
            ],
            unknown=[
                UC( 5 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
        LC( loadid='lid2',
            exacts=[
                EC( 'A', 200 )
            ],
            partial=[
            ],
            unknown=[
                UC( 25 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C' } ),
        LC( loadid='lid3',
            exacts=[
            ],
            partial=[
                PC( frozenset( ('A', 'B') ), count=40 )
            ],
            unknown=[
                UC( 25 ),
                UC( 10 ),
            ],
            tags={ 'A', 'B', 'C', 'D' } ),
    ] )

    load_eq_class = cp1.compute_load_eq_class()

    assert set( load_eq_class.__iter__() ) == { 'lid1', 'lid2', 'lid3' }
    assert load_eq_class.check_equivalence( 'lid1', 'lid2' )
    assert len( set( load_eq_class.classes() ) ) == 2

    witness = load_eq_class.witness( 'lid1' )

    cp_test = cp1.apply_eqclass_loads( load_eq_class )
    cp_ref = CP( [
        LC( loadid=witness,
            exacts=[
                EC( 'A', 300 ),
            ],
            partial=[
            ],
            unknown=[
                UC( 50 ),
            ],
            tags={ 'A', 'B', 'C' } ),
        LC( loadid='lid3',
            exacts=[
            ],
            partial=[
                PC( frozenset( ('A', 'B') ), count=40 )
            ],
            unknown=[
                UC( 35 ),
            ],
            tags={ 'A', 'B', 'C', 'D' } ),
    ] )

    assert_eq_context_path( cp_test, cp_ref )


def test_context_path_tag_eq_class() :
    cp1 = CP( [
        LC( loadid='lc1',
            tags={ 'A', 'B', 'C' } ),
        LC( loadid='lc2',
            tags={ 'A', 'C', 'D' } ),
        LC( loadid='lc3',
            tags={ 'A', 'E', 'C' } )
    ] )

    eq_class_tags = cp1.compute_tag_eq_class()

    assert set( eq_class_tags.__iter__() ) == { 'A', 'B', 'C', 'D', 'E' }
    assert eq_class_tags.check_equivalence( 'A', 'C' )
    assert len( set( eq_class_tags.classes() ) ) == 4

    witness = eq_class_tags.witness( 'A' )

    cp1 = CP( [
        LC( loadid='lc1',
            exacts=[
                EC( 'A', 10 ),
                EC( 'B', 20 )
            ],
            partial=[
                PC( frozenset( [ 'A', 'C' ] ), 10 ),
                PC( frozenset( [ 'A', 'D' ] ), 20 )
            ],
            tags={ 'A', 'B', 'C', 'D' } ),
        LC( loadid='lc2',
            exacts=[
                EC( 'A', 10 ),
                EC( 'C', 20 )
            ],
            partial=[
                PC( frozenset( [ 'D', 'C' ] ), 10 ),
                PC( frozenset( [ 'B', 'D' ] ), 20 )
            ],
            tags={ 'A', 'B', 'C', 'D' } ),
        LC( loadid='lc3',
            exacts=[
                EC( 'A', 10 ),
                EC( 'C', 20 )
            ],
            partial=[
            ],
            unknown=[
                UC( 20 )
            ],
            tags={ 'A', 'C' } )
    ] )

    cp_test = cp1.apply_eqclass_tags( eq_class_tags )
    cp_ref = CP( [
        LC( loadid='lc1',
            exacts=[
                EC( witness, 10 ),
                EC( 'B', 20 )
            ],
            partial=[
                PC( frozenset( [ witness ] ), 10 ),
                PC( frozenset( [ witness, 'D' ] ), 20 )
            ],
            tags={ witness, 'B', 'D' } ),
        LC( loadid='lc2',
            exacts=[
                EC( witness, 10 ),
                EC( witness, 20 )
            ],
            partial=[
                PC( frozenset( [ 'D', witness ] ), 10 ),
                PC( frozenset( [ 'B', 'D' ] ), 20 )
            ],
            tags={ witness, 'B', 'D' } ),
        LC( loadid='lc3',
            exacts=[
                EC( witness, 10 ),
                EC( witness, 20 )
            ],
            partial=[
            ],
            unknown=[
                UC( 20 )
            ],
            tags={ witness } )
    ] )

    assert_eq_context_path( cp_test, cp_ref )


def test_ilp_interval_problem_basic() :
    cp1: ContextPath = CP( [
        LC( loadid='lid1', tags={ 'A', 'B', 'C' } ),
        LC( loadid='lid2', tags={ 'A', 'E', 'C' } ),
        LC( loadid='lid3', tags={ 'A', 'F' } ),
    ] )
    cp2: ContextPath = CP( [
        LC( loadid='lid4', tags={ 'A', 'T' } ),
        LC( loadid='lid5', tags={ 'A', 'D' } ),
        LC( loadid='lid6', tags={ 'H', 'G' } ),
    ] )
    cp3: ContextPath = CP( [
        LC( loadid='lid7', tags={ 'O', 'P' } ),
        LC( loadid='lid8', tags={ 'H', 'L' } ),
    ] )
    iip1 = IIP(
        path_contexts=cp1,
        constraints=[
            (cp2, 10),
            (cp3, 10)
        ],
        eqclass_tags=EqClassMap(),
        eqclass_loads=EqClassMap()
    )

    assert iip1.extract_tag_set() == { 'A', 'B', 'C', 'E', 'F', 'D', 'T', 'H', 'G', 'O', 'P', 'L' }

    for pc in iip1.load_traversal() :
        assert pc.loadid in cp1 or pc.loadid in cp2 or pc.loadid in cp3

    assert len( list( iip1.load_traversal() ) ) == len( cp1 ) + len( cp2 ) + len( cp3 )

    for path in iip1.path_traversal() :
        assert path is cp1 or path is cp2 or path is cp3

    assert len( list( iip1.path_traversal() ) ) == 3


def test_iip_remove_unopti() :
    # Remove unoptimizable
    cp1 = CP( [
        LC( loadid='lid1', tags={ 'A', 'B', 'C' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid2', tags={ 'A', 'E', 'C' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid3', tags={ 'A' }, unknown=[ UC( 10 ) ] ),
    ] )
    cp2 = CP( [
        LC( loadid='lid4', tags={ 'A', 'T' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid5', tags={ 'A', 'D' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid6', tags={ 'H', 'G' }, unknown=[ UC( 10 ) ] ),
    ] )
    cp3 = CP( [
        LC( loadid='lid7', tags={ 'O', 'P' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid8', tags={ 'H', 'L' }, unknown=[ UC( 10 ) ] ),
    ] )
    cp1_ref = CP( [
        LC( loadid='lid1', tags={ 'A', 'B', 'C' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid2', tags={ 'A', 'E', 'C' }, unknown=[ UC( 10 ) ] ),
    ] )
    iip1 = IIP(
        path_contexts=cp1,
        constraints=[
            (cp2, 10),
            (cp3, 10)
        ],
        eqclass_tags=EqClassMap(),
        eqclass_loads=EqClassMap()
    )

    iip_test = iip1.remove_unoptimizable_contexts()
    iip_ref = IIP(
        path_contexts=cp1_ref,
        constraints=[
            (cp2, 10),
            (cp3, 10)
        ],
        eqclass_tags=EqClassMap(),
        eqclass_loads=EqClassMap()
    )

    assert_eq_iip( iip_test, iip_ref )


def test_iip_inner_context_fusion() :
    # Inner context fusion
    cp1 = CP( [
        LC( loadid='lid1', tags={ 'A', 'B', 'C' }, unknown=[ UC( 10 ), UC( 20 ) ] ),
        LC( loadid='lid2', tags={ 'A', 'E', 'C' }, unknown=[ UC( 10 ) ] ),
    ] )
    cp2 = CP( [
        LC( loadid='lid4', tags={ 'A', 'T' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid5', tags={ 'A', 'D' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid6', tags={ 'H', 'G' }, unknown=[ UC( 10 ) ] ),
    ] )
    cp3 = CP( [
        LC( loadid='lid7', tags={ 'O', 'P' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid8', tags={ 'H', 'L' }, unknown=[ UC( 10 ) ] ),
    ] )
    cp1_ref = CP( [
        LC( loadid='lid1', tags={ 'A', 'B', 'C' }, unknown=[ UC( 30 ) ] ),
        LC( loadid='lid2', tags={ 'A', 'E', 'C' }, unknown=[ UC( 10 ) ] ),
    ] )
    iip1 = IIP(
        path_contexts=cp1,
        constraints=[
            (cp2, 10),
            (cp3, 10)
        ],
        eqclass_tags=EqClassMap(),
        eqclass_loads=EqClassMap()
    )

    iip_test = iip1.inner_contexts_fusion()
    iip_ref = IIP(
        path_contexts=cp1_ref,
        constraints=[
            (cp2, 10),
            (cp3, 10)
        ],
        eqclass_tags=EqClassMap(),
        eqclass_loads=EqClassMap()
    )

    assert_eq_iip( iip_test, iip_ref )


def test_iip_tag_eqclass() :
    # Tag equivalent class
    cp1 = CP( [
        LC( loadid='lid1', tags={ 'A', 'B', 'C' }, unknown=[ UC( 10 ), UC( 20 ) ] ),
        LC( loadid='lid2', tags={ 'A', 'E', 'C' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid3', tags={ 'D', 'F', 'E' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid4', tags={ 'D', 'F', 'B' }, unknown=[ UC( 10 ) ] ),
    ] )
    cp2 = CP( [
        LC( loadid='lid4', tags={ 'A', 'F', 'C' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid5', tags={ 'A', 'D', 'C' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid6', tags={ 'H', 'P' }, unknown=[ UC( 10 ) ] ),
    ] )
    cp3 = CP( [
        LC( loadid='lid7', tags={ 'O', 'P' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid8', tags={ 'H', 'L' }, unknown=[ UC( 10 ) ] ),
    ] )

    iip1 = IIP(
        path_contexts=cp1,
        constraints=[
            (cp2, 10),
            (cp3, 10)
        ],
        eqclass_tags=EqClassMap(),
        eqclass_loads=EqClassMap()
    )

    iip_test = iip1.tag_equivalent_class()
    tag_eq_class_ref = EqClassMap( { 'A', 'B', 'C', 'E', 'D', 'F', 'H', 'O', 'P', 'L' } )
    tag_eq_class_ref.fusion_element_class( 'A', 'C' )

    assert_eq_eqclass( iip_test.eqclass_tags, tag_eq_class_ref )
    assert_eq_eqclass( iip_test.eqclass_loads, EqClassMap() )

    witnessA = iip_test.eqclass_tags.witness( 'A' )

    cp1_ref = CP( [
        LC( loadid='lid1', tags={ witnessA, 'B' }, unknown=[ UC( 10 ), UC( 20 ) ] ),
        LC( loadid='lid2', tags={ witnessA, 'E' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid3', tags={ 'D', 'F', 'E' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid4', tags={ 'D', 'F', 'B' }, unknown=[ UC( 10 ) ] ),
    ] )
    cp2_ref = CP( [
        LC( loadid='lid4', tags={ witnessA, 'F' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid5', tags={ witnessA, 'D' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid6', tags={ 'H', 'P' }, unknown=[ UC( 10 ) ] ),
    ] )
    cp3_ref = CP( [
        LC( loadid='lid7', tags={ 'O', 'P' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid8', tags={ 'H', 'L' }, unknown=[ UC( 10 ) ] ),
    ] )

    iip_ref = IIP(
        path_contexts=cp1_ref,
        constraints=[
            (cp2_ref, 10),
            (cp3_ref, 10)
        ],
        eqclass_tags=iip_test.eqclass_tags,
        eqclass_loads=EqClassMap()
    )

    assert_eq_iip( iip_test, iip_ref )


def test_iip_eqclass_load() :
    # Load equivalent class
    cp1 = CP( [
        LC( loadid='lid1', tags={ 'A', 'B', 'C' }, unknown=[ UC( 10 ), UC( 20 ) ] ),
        LC( loadid='lid2', tags={ 'A', 'B', 'C' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid3', tags={ 'D', 'F', 'E' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid4', tags={ 'D', 'F', 'E' }, unknown=[ UC( 10 ) ] ),
    ] )
    cp2 = CP( [
        LC( loadid='lid1', tags={ 'A', 'B', 'C' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid2', tags={ 'A', 'B', 'C' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid3', tags={ 'D', 'F', 'E' }, unknown=[ UC( 10 ) ] ),
    ] )
    cp3 = CP( [
        LC( loadid='lid7', tags={ 'O', 'P' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid8', tags={ 'H', 'L' }, unknown=[ UC( 10 ) ] ),
    ] )

    iip1 = IIP(
        path_contexts=cp1,
        constraints=[
            (cp2, 10),
            (cp3, 10)
        ],
        eqclass_tags=EqClassMap(),
        eqclass_loads=EqClassMap()
    )

    iip_test = iip1.load_equivalent_class()
    load_eq_class_ref = EqClassMap( { 'lid1', 'lid2', 'lid3', 'lid4', 'lid7', 'lid8' } )
    load_eq_class_ref.fusion_element_class( 'lid1', 'lid2' )

    assert_eq_eqclass( iip_test.eqclass_loads, load_eq_class_ref )
    assert_eq_eqclass( iip_test.eqclass_tags, EqClassMap() )

    witness = iip_test.eqclass_loads.witness( 'lid1' )

    cp1_ref = CP( [
        LC( loadid=witness, tags={ 'A', 'B', 'C' }, unknown=[ UC( 40 ) ] ),
        LC( loadid='lid3', tags={ 'D', 'F', 'E' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid4', tags={ 'D', 'F', 'E' }, unknown=[ UC( 10 ) ] ),
    ] )
    cp2_ref = CP( [
        LC( loadid=witness, tags={ 'A', 'B', 'C' }, unknown=[ UC( 20 ) ] ),
        LC( loadid='lid3', tags={ 'D', 'F', 'E' }, unknown=[ UC( 10 ) ] ),
    ] )
    cp3_ref = CP( [
        LC( loadid='lid7', tags={ 'O', 'P' }, unknown=[ UC( 10 ) ] ),
        LC( loadid='lid8', tags={ 'H', 'L' }, unknown=[ UC( 10 ) ] ),
    ] )
    iip_ref = IIP(
        path_contexts=cp1_ref,
        constraints=[
            (cp2_ref, 10),
            (cp3_ref, 10)
        ],
        eqclass_tags=EqClassMap(),
        eqclass_loads=load_eq_class_ref
    )

    assert_eq_iip( iip_test, iip_ref )
