# coding=utf-8
import pytest

from optDfi.optimizations.ilp_opt.ilp_problem.equivalence_map import *


def test_eqclass() :
    o1: EqClass[ int ] = EqClass( (1, 2, 3, 4, 5), witness=1 )
    o2: EqClass[ int ] = EqClass( (8, 7, 6), witness=6 )

    assert o1 != o2

    o1.union( o2 )

    assert o1.data == EqData({ 1, 2, 3, 4, 5, 6, 7, 8 }, witness=1 )
    assert o1 == o2

    o3: EqClass[ int ] = EqClass( (1, 2, 3, 4, 5, 6, 7, 8), witness=1 )

    assert o1 == o3
    assert o2 == o3

    assert len( o1 ) == 8
    assert 1 in EqClass( (1, 2, 3) )
    assert 4 not in EqClass( (1, 2, 3) )

    assert set( o3.__iter__() ) == { 1, 2, 3, 4, 5, 6, 7, 8 }


def test_eqclass_fusion() :
    o1 = EqClass( (1, 2, 3), witness=1 )
    o2 = EqClass( (4, 5), witness=4 )

    o1.union( o2 )

    assert o1.data is o2.data
    assert o1.data == EqData({ 1, 2, 3, 4, 5 }, witness=1)


def test_eqclassmap_init() :
    map1: EqClassMap[ int ] = EqClassMap( (1, 2, 3, 7) )

    # Check the __init__ works normally
    assert map1.current_cls == { 1, 2, 3, 7 }
    for t in map1.map :
        assert map1.map[ t ].data == EqData({ t }, witness=t)

    # Check the __contains__
    assert 1 in map1
    assert 8 not in map1

    # Check the __getitem__ and __setitem__
    assert map1[ 1 ] == map1.map[ 1 ].data.equivalent_set

    with pytest.raises( Exception, match=r".*: 1" ) :
        map1[ 1 ] = { 5 }

    # Check the __iter__
    assert set( map1 ) == { 1, 2, 3, 7 }

    # Check add_elements
    map1.add_elements( (5, 8) )
    assert 5 in map1
    assert 8 in map1
    assert not map1.check_equivalence( 5, 8 )
    assert not map1.check_equivalence( 5, 1 )


def test_eqclassmap_element_fusion() :
    map1: EqClassMap[ int ] = EqClassMap( (1, 2, 3, 4) )

    assert set( map1.classes() ) == { 1, 2, 3, 4 }
    for t in map1 :
        assert map1.check_equivalence( t, 1 ) == (t == 1)

    map1.fusion_element_class( 1, 2 )
    map1.fusion_element_class( 1, 4 )

    assert set( map1.classes() ) == { 1, 3 }
    assert map1.check_equivalence( 1, 2 )
    assert not map1.check_equivalence( 1, 3 )
    assert map1.check_equivalence( 1, 4 )
    assert map1.map[ 1 ].data is map1.map[ 2 ].data

    map1.fusion_element_class( 3, 3 )

    assert 3 in set(map1.classes())
    assert map1[3] == {3}


def test_eqclassmap_and() :
    map1 = EqClassMap( (1, 2, 3, 4, 5, 6, 7, 8) )
    map2 = EqClassMap( (1, 2, 3, 4, 9, 10, 11) )

    map1.fusion_element_class( 1, 2 )
    map1.fusion_element_class( 1, 3 )
    map1.fusion_element_class( 1, 5 )
    map1.fusion_element_class( 6, 7 )

    map2.fusion_element_class( 1, 3 )
    map2.fusion_element_class( 10, 11 )

    new_map = map1 & map2

    for i in range( 1, 12 ) :
        assert i in new_map

    assert new_map.check_equivalence( 1, 3 )
    assert new_map.check_equivalence( 6, 7 )
    assert new_map.check_equivalence( 10, 11 )
    assert not new_map.check_equivalence( 1, 2 )
    assert not new_map.check_equivalence( 1, 5 )
    assert not new_map.check_equivalence( 2, 8 )
    assert not new_map.check_equivalence( 3, 9 )

    map1.fusion_element_class( 3, 4 )
    map2.fusion_element_class( 3, 4 )
    assert not new_map.check_equivalence( 3, 4 )
    assert set( new_map.classes() ) == { 1, 2, 4, 5, 6, 8, 9, 10 }
